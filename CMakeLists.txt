cmake_minimum_required(VERSION 3.13 FATAL_ERROR)

if (NOT UNIX)
cmake_policy(SET CMP0091 NEW)
set (CMAKE_SYSTEM_VERSION 8.1 CACHE TYPE INTERNAL FORCE)
endif()

project(mlds)

set(HDF5_USE_STATIC_LIBRARIES ON)

include(GNUInstallDirs)
include(InstallRequiredSystemLibraries)
include(FetchContent)

set(GPU CACHE BOOL OFF)
set(INTERNAL_PYTORCH CACHE BOOL ON)
set(LOCAL_TORCH_DIR CACHE STRING "")
set(MLDS_VERSION "9.90")
set(PYTORCH_BRANCH "release/1.9")

execute_process(COMMAND g++ --print-file-name=libgfortran.a 
                OUTPUT_VARIABLE GFORTRAN_LIB 
                OUTPUT_STRIP_TRAILING_WHITESPACE)
message("GFortran static library: ${GFORTRAN_LIB}")
execute_process(COMMAND g++ --print-file-name=librt.a
                OUTPUT_VARIABLE RT_LIB
                OUTPUT_STRIP_TRAILING_WHITESPACE)
message("librt static library: ${RT_LIB}")
execute_process(COMMAND g++ --print-file-name=libnuma.a
                OUTPUT_VARIABLE NUMA_LIB
                OUTPUT_STRIP_TRAILING_WHITESPACE)
message("NUMA static library: ${NUMA_LIB}")
if (${CMAKE_SYSTEM_PROCESSOR} MATCHES x86_64)
execute_process(COMMAND g++ --print-file-name=libquadmath.a
                OUTPUT_VARIABLE QUAD_LIB
                OUTPUT_STRIP_TRAILING_WHITESPACE)
else()
set(QUAD_LIB "")
endif()
message("QUADMATH static library: ${QUAD_LIB}")
execute_process(COMMAND g++ --print-file-name=libgomp.a
                OUTPUT_VARIABLE OMP_LIB
                OUTPUT_STRIP_TRAILING_WHITESPACE)
message("OMP static library: ${OMP_LIB}")

#if (UNIX)
set(CMAKE_PREFIX_PATH ${CMAKE_CURRENT_BINARY_DIR}/extern/;${CMAKE_PREFIX_PATH})
#endif()
add_subdirectory(extern)

find_package(Python3)

#
#set(BUILD_SHARED_LIBS OFF)
#set(DYNAMIC_ARCH ON)
#set(DYNAMIC_OLDER ON)
#set(NO_AFFINITY ON)
#set(BUILD_BFLOAT16 ON)
#
set(BUILD_SHARED_LIBS OFF)
set(USE_MKLDNN OFF)
set(USE_CUDA OFF)
set(USE_DISTRIBUTED OFF)
set(BUILD_PYTHON OFF)
set(USE_ROCM OFF)
set(USE_NCCL OFF)
set(BUILD_CAFFE2_OPS OFF)
set(BLAS_INFO "open")
set(WITH_BLAS "open")
#-DBLAS_LIBRARIES=-Wl,--no-whole-archive,/home/john/git/mlds/build/extern/lib/libopenblas.a\ -lpthread
#set(USE_ROCM OFF)

if (NOT INTERNAL_PYTORCH)
	set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH};${LOCAL_TORCH_DIR}")
	find_package(Torch REQUIRED)
if(USE_ROCM)
	find_package(hipsparse REQUIRED)
	find_package(rocblas REQUIRED)
	find_package(hiprand REQUIRED)
	find_package(rocfft REQUIRED)
endif(USE_ROCM)

endif()





if (UNIX)
if (INTERNAL_PYTORCH)
        set(TORCH_LIB_DIR "${CMAKE_CURRENT_BINARY_DIR}/extern/lib")
        set(TORCH_INTERNAL_LIB_DIR "${CMAKE_CURRENT_BINARY_DIR}/extern/pytorch-prefix/src/pytorch-build/lib")
        list(APPEND TORCH_LIBS "-Wl,--whole-archive,${TORCH_LIB_DIR}/libtorch.a ${TORCH_LIB_DIR}/libtorch_cpu.a")
        list(APPEND TORCH_LIBS "-Wl,--no-whole-archive,${TORCH_LIB_DIR}/libc10.a")
if (NOT ${CMAKE_SYSTEM_PROCESSOR} MATCHES ppc64le)
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libnnpack.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libqnnpack.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libpytorch_qnnpack.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libXNNPACK.a")
endif()
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libcaffe2_protos.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libprotobuf-lite.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libprotobuf.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libprotoc.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libclog.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libfbgemm.a")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libsleef.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libasmjit.a")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libonnx.a")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libonnx_proto.a")
if (${CMAKE_SYSTEM_PROCESSOR} MATCHES x86_64)
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libfbgemm.a")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libasmjit.a")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libCaffe2_perfkernels_avx.a")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libCaffe2_perfkernels_avx2.a")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libCaffe2_perfkernels_avx512.a")
endif()
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libfoxi_loader.a")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libkineto.a")        
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libcpuinfo.a")
if (NOT ${CMAKE_SYSTEM_PROCESSOR} MATCHES ppc64le)
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libpthreadpool.a")
endif()
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libopenblas.a")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libclog.a")
        list(JOIN TORCH_LIBS " " TORCH_LIBRARIES)

	#set(TORCH_CXX_FLAGS "-D_GLIBCXX_USE_CXX11_ABI=1")
	set(TORCH_INCLUDE_DIRS "${CMAKE_CURRENT_BINARY_DIR}/extern/include" "${CMAKE_CURRENT_BINARY_DIR}/extern/include/torch/csrc/api/include")
	#set(TORCH_LIBRARY "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libtorch.so")
	#set(TORCH_CPU_LIBRARY "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libtorch_cpu.so")
	#set(C10_LIBRARY "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libc10.so")
	#set(TORCH_LIBRARIES "${TORCH_LIBRARY}" "${C10_LIBRARY}" "${TORCH_CPU_LIBRARY}")
if (GPU)
if (USE_CUDA)
	find_package(CUDA REQUIRED)
	set(TORCH_CUDA_LIBRARY "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libtorch_cuda.so")
	set(C10_CUDA_LIBRARY "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libc10_cuda.so")
	set(TORCH_LIBRARIES "${TORCH_LIBRARIES}" "${TORCH_CUDA_LIBRARY}" "${C10_CUDA_LIBRARY}" "${CUDA_LIBRARIES}")
	set(TORCH_INCLUDE_DIRS "${TORCH_INCLUDE_DIRS}" "${CUDA_INCLUDE_DIRS}")
endif(USE_CUDA)
endif(GPU)
endif(INTERNAL_PYTORCH)
	set(BOINC_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/extern/include)
	set(BOINC ${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libboinc.a)
	set(BOINC_API ${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libboinc_api.a)
	set(BOINC_GRAPHICS ${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libboinc_graphics2.a)

	set(LINUX_DEPLOY ${CMAKE_CURRENT_BINARY_DIR}/extern/bin/linuxdeploy)
else(UNIX)
	# Windows

	#set(VCPKG_TARGET_TRIPLET "x64-windows-static")
	#set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded")
	#set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH};c:/build/vcpkg/installed/x64-windows-static")
	#set(BOINC_INSTALL_PREFIX "c:/build/git/boinc/")
	#set(BOINC_INCLUDE_DIR "${BOINC_INSTALL_PREFIX}/api;${BOINC_INSTALL_PREFIX}/lib")
	#set(BOINC_LIB_DIR "${BOINC_INSTALL_PREFIX}/win_build/Build/x64/Release")
	#set(BOINC ${BOINC_LIB_DIR}/libboinc.lib)
	#set(BOINC_API ${BOINC_LIB_DIR}/libboincapi.lib)
	#set(BOINC_GRAPHICS ${BOINC_LIB_DIR}/libgraphics2.lib)

        set(TORCH_LIB_DIR "${CMAKE_CURRENT_BINARY_DIR}/extern/lib")
	set(TORCH_INTERNAL_LIB_DIR "${CMAKE_CURRENT_BINARY_DIR}/extern/pytorch-prefix/src/pytorch-build/lib/Release")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/torch.lib")
	list(APPEND TORCH_LIBS "-WHOLEARCHIVE:${TORCH_LIB_DIR}/torch_cpu.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/c10.lib")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libnnpack.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libqnnpack.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libpytorch_qnnpack.a")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/XNNPACK.lib")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/torch_global_deps.lib")
	list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/caffe2_protos.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libprotobuf-lite.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libprotobuf.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libprotoc.lib")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libfbgemm.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libsleef.a")
	#list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/libasmjit.a")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/onnx.lib")
        list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/onnx_proto.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/fbgemm.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/asmjit.lib")
	list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/Caffe2_perfkernels_avx.lib")
	list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/Caffe2_perfkernels_avx2.lib")
	list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/Caffe2_perfkernels_avx512.lib")
	#list(APPEND TORCH_LIBS "${TORCH_INTERNAL_LIB_DIR}/libfoxi_loader.a")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/kineto.lib")        
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/cpuinfo.lib")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/pthreadpool.lib")
        list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/openblas.lib")
	list(APPEND TORCH_LIBS "${TORCH_LIB_DIR}/clog.lib")
	#list(JOIN TORCH_LIBS ";" TORCH_LIBRARIES)

	#set(TORCH_CXX_FLAGS "-D_GLIBCXX_USE_CXX11_ABI=1")
	#set(TORCH_INCLUDE_DIRS "${CMAKE_CURRENT_BINARY_DIR}/extern/include" "${CMAKE_CURRENT_BINARY_DIR}/extern/include/torch/csrc/api/include")
	
endif(UNIX)

set(HDF5_USE_STATIC_LIBRARIES ON)
if (MSVC)
set(HDF5_DIR "c:/build/vcpkg/installed/x64-windows-static/share/hdf5")
endif(MSVC)
#find_package(HDF5 COMPONENTS CXX REQUIRED)

set(HDF5_CXX_INCLUDE_DIR "${CMAKE_CURRENT_BINARY_DIR}/extern/include")

if (NOT MSVC)
set(HDF5_CXX_LIBRARIES "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5_cpp.a"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5_hl_cpp.a"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5_hl.a"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5.a"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libz.a")
else(NOT MSVC)
set(HDF5_CXX_LIBRARIES "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5_cpp.lib"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5_hl_cpp.lib"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5_hl.lib"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libhdf5.lib"
                       "${CMAKE_CURRENT_BINARY_DIR}/extern/lib/zlibstatic.lib")
endif(NOT MSVC)
                        
#
# Wrapper
#
if(UNIX)
add_executable(wrapper_${CMAKE_SYSTEM_PROCESSOR} ${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/samples/wrapper/wrapper.cpp 
		 ${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/samples/wrapper/regexp.c 
		 ${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/samples/wrapper/regsub.c 
		 ${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/samples/wrapper/regerror.c 
		 ${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/samples/wrapper/regexp_memory.c 
		 ${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/samples/wrapper/regexp_report.c)
add_dependencies(wrapper_${CMAKE_SYSTEM_PROCESSOR} boinc)
target_include_directories(wrapper_${CMAKE_SYSTEM_PROCESSOR} PRIVATE ${BOINC_INCLUDE_DIR} 
	${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/
	${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/api 
	${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/lib 
	${CMAKE_CURRENT_SOURCE_DIR}/extern/boinc/zip )
target_link_options(wrapper_${CMAKE_SYSTEM_PROCESSOR} PUBLIC "-static" "-static-libgcc" "-static-libstdc++")
target_link_libraries(wrapper_${CMAKE_SYSTEM_PROCESSOR} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libboinc_api.a
			${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libboinc.a
			${CMAKE_CURRENT_BINARY_DIR}/extern/lib/libboinc_zip.a -lpthread)
endif(UNIX)

#
# MLDS
#
add_executable(mlds 
	src/main.cpp 
	src/dataset.cpp 
	src/llog.cpp 
	src/status.cpp)
add_dependencies(mlds hdf5)
if (INTERNAL_PYTORCH)
add_dependencies(mlds pytorch)
endif(INTERNAL_PYTORCH)
set_property(TARGET mlds PROPERTY CXX_STANDARD 17)
set_property(TARGET mlds PROPERTY INSTALL_RPATH "$ORIGIN/")
if (MSVC)
	#set_property(TARGET mlds PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded")
endif()
target_compile_options(mlds PRIVATE "${TORCH_CXX_FLAGS}" -fopenmp ) 
target_compile_definitions(mlds PUBLIC 
	MLDS_VERSION=\"${MLDS_VERSION}\"
	PYTORCH_BRANCH=\"${PYTORCH_BRANCH}\"
)
if (GPU)
	target_compile_options(mlds PRIVATE "-DGPU")
if (USE_ROCM)
	target_compile_options(mlds PRIVATE "-DROCM")
	target_compile_options(mlds PRIVATE "-D__HIP_PLATFORM_NVCC__")
	target_include_directories(mlds PRIVATE /opt/rocm/include)
endif(USE_ROCM)
endif(GPU)

if (MSVC)
	target_compile_options(mlds PRIVATE
		"-DWINVER=0x0601"
		"-D_WIN32_WINNT=0x0601"
	)
endif()

target_include_directories(mlds PUBLIC 
	${TORCH_INCLUDE_DIRS} 
	${TORCH_INCLUDE_DIR}
	${HDF5_CXX_INCLUDE_DIR} 
	${CMAKE_CURRENT_SOURCE_DIR}/src 
)
if (NOT MSVC)
target_link_libraries(mlds PRIVATE -lm
	"${TORCH_LIBRARIES}" 
	"${HDF5_CXX_LIBRARIES}" 
	"${HDF5_LIBRARIES}" 
        "${GFORTRAN_LIB}" 
        "${RT_LIB}" 
        "${NUMA_LIB}"
        "${QUAD_LIB}"
        "${OMP_LIB}"
	-ldl -lpthread -latomic
)
else(NOT MSVC)
target_link_libraries(mlds PRIVATE 
	${TORCH_LIBRARIES} 
	${HDF5_CXX_LIBRARIES}
	${HDF5_LIBRARIES}
)
endif(NOT MSVC)


if (UNIX)
	target_link_libraries(mlds PRIVATE stdc++fs)
	target_link_options(mlds PUBLIC
		"-static"
		"-static-libgcc"
		"-static-libstdc++"
	)
#	target_link_libraries(mlds_extract_weights PRIVATE stdc++fs)
#	target_link_options(mlds_extract_weights PUBLIC
#		"-static"
#		"-static-libgcc"
#		"-static-libstdc++"
#       )
endif()

#
# Prints out all cmake variables. Useful snippet
#
#get_cmake_property(_variableNames VARIABLES)
#list (SORT _variableNames)
#foreach (_variableName ${_variableNames})
#	    message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()

if (UNIX)
#
# mlds_val_helper
#
add_executable(mlds_val_helper 
	src/mlds_val_helper.cpp 
	src/dataset.cpp 
	src/llog.cpp
	src/status.cpp
)
if (INTERNAL_PYTORCH)
add_dependencies(mlds_val_helper pytorch)
endif()
target_compile_options(mlds_val_helper PRIVATE
	"${TORCH_CXX_FLAGS}" -fopenmp
)
target_include_directories(mlds_val_helper PUBLIC
	${TORCH_INCLUDE_DIRS}
        ${HDF5_CXX_INCLUDE_DIRS}
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)
target_link_libraries(mlds_val_helper
        "${TORCH_LIBRARIES}"
        "${HDF5_CXX_LIBRARIES}"
        "${HDF5_LIBRAIES}"
	"${RT_LIB}"
	"${NUMA_LIB}"
	"${OMP_LIB}"
	-ldl -lpthread 
        stdc++fs
)

target_link_options(mlds_val_helper PUBLIC
	"-static"
	"-static-libgcc"
	"-static-libstdc++"
)


#
# mlds_extract_weights
#
add_executable(mlds_extract_weights
	src/mlds_extract_weights.cpp
	src/llog.cpp
	src/status.cpp
)
if (INTERNAL_PYTORCH)
add_dependencies(mlds_extract_weights pytorch)
endif()
set_property(TARGET mlds_extract_weights PROPERTY CXX_STANDARD 17)
target_compile_options(mlds_extract_weights PRIVATE
	"${TORCH_CXX_FLAGS}"
)       
target_include_directories(mlds_extract_weights PUBLIC
	${TORCH_INCLUDE_DIRS}
	${CMAKE_CURRENT_SOURCE_DIR}/src
)
target_link_libraries(mlds_extract_weights
       "${TORCH_LIBRARIES}"
        "${GFORTRAN_LIB}"
        "${RT_LIB}"
        "${NUMA_LIB}"
        "${QUAD_LIB}"
        "${OMP_LIB}"
	-ldl -lpthread
	stdc++fs
)
target_link_options(mlds_extract_weights PUBLIC
        "-static"
        "-static-libgcc"
        "-static-libstdc++"
)							              

endif()

# The following code block is suggested to be used on Windows.
# According to https://github.com/pytorch/pytorch/issues/25457,
# the DLLs need to be copied to avoid memory errors.
if (MSVC)
  file(GLOB TORCH_DLLS "${TORCH_INSTALL_PREFIX}/lib/*.dll")
  file(GLOB TORCH_DLLS2 "${TORCH_INSTALL_PREFIX}/bin/*.dll")
  add_custom_command(TARGET mlds
                     POST_BUILD
                     COMMAND ${CMAKE_COMMAND} -E copy_if_different
                     ${TORCH_DLLS}
                     $<TARGET_FILE_DIR:mlds>)
     install(PROGRAMS ${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS} ${TORCH_DLLS} ${TORCH_DLLS2}
	        DESTINATION bin
		COMPONENT applications)
endif (MSVC)

SET(CMAKE_SKIP_BUILD_RPATH  FALSE)
SET(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
SET(CMAKE_INSTALL_RPATH "$\{ORIGIN\}")
install(TARGETS mlds RUNTIME DESTINATION . COMPONENT applications)

if (GPU)
set(CPACK_PACKAGE_NAME "mlds-gpu")
else(GPU)
set(CPACK_PACKAGE_NAME "mlds")
endif(GPU)
set(CPACK_PACKAGE_VENDOR "www.mlcathome.org")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Machine Learning Dataset Generator")
set(CPACK_PACKAGE_VERSION "9.90")
set(CPACK_PACKAGE_VERSION_MAJOR "9")
set(CPACK_PACKAGE_VERSION_MINOR "9")
set(CPACK_PACKAGE_VERSION_PATCH "0")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "MLDS")
set(CPACK_MONOLITHIC_INSTALL TRUE)
set(CPACK_STRIP_FILES TRUE)
set(CPACK_INSTALL_OPENMP_LIBRARIES TRUE)
include(CPack)
