pushd "c:\Program` Files` (x86)\Microsoft` Visual` Studio\2019\Community\VC\Auxiliary\Build"
cmd /c "vcvars64.bat & set"|
foreach {
  if ($_ -match "=") {
      $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd
c:\ProgramData\Miniconda3\Scripts\conda init powershell
c:\ProgramData\Miniconda3\Scripts\conda activate c:\build\mlcbuild
