import sys
import os
import csv
import h5py
import numpy as np


Xt = []
Yt = []
Xv = []
Yv = []

with open(sys.argv[1], 'r') as fp:
    csvrdr = csv.reader(fp)
    for row in csvrdr:
        y = [0.]*10
        y[int(row[0])] = 1.0
        Yt.append(y)
        Xt.append([float(x)/255.0 for x in row[1:]])

with open(sys.argv[2], 'r') as fp:
    csvrdr = csv.reader(fp)
    for row in csvrdr:
        y = [0.]*10
        y[int(row[0])] = 1.0
        Yv.append(y)
        Xv.append([float(x)/255.0 for x in row[1:]])

Xe = Xt[50000:]
Ye = Yt[50000:]
Xt = Xt[:50000]
Yt = Yt[:50000]

Xt,Yt,Xv,Yv,Xe,Ye = [np.array(x) for x in [Xt,Yt,Xv,Yv,Xe,Ye]]

with h5py.File(sys.argv[3]+'-train-val-dataset.hdf5', 'w') as h:
    h.create_dataset('Xt', data=Xt, dtype='float32', compression='gzip')
    h.create_dataset('Yt', data=Yt, dtype='float32', compression='gzip')
    h.create_dataset('Xv', data=Xv, dtype='float32', compression='gzip')
    h.create_dataset('Yv', data=Yv, dtype='float32', compression='gzip')
    
    
with h5py.File(sys.argv[3]+'-eval-dataset.hdf5', 'w') as h:
    h.create_dataset('Xs', data=Xe, dtype='float32', compression='gzip')
    h.create_dataset('Ys', data=Ye, dtype='float32', compression='gzip')

