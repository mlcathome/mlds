#!/usr/bin/env python3
#
# bitmachines.py: A collection of simple 8-bit wide device models 
#                 to test simple neurodev models. 
#
# Copyright (c) 2018-2020 John Clemens <clemej1@umbc.edu>
#

import random
import numpy as np
import copy
import unittest

#
# Simple machine that returns the last value written on input to
# the output.
#
class BitMachine():
        def __init__(self, width):
                self.inputs = [0]*width
                self.width = width

        def set_input(self, r):
                self.inputs[r] = 1

        def clear_input(self, r):
                self.inputs[r] = 0

        def get_input(self, r):
                return self.inputs[r]

        def output(self):
                return copy.copy(self.inputs)

        def reset(self):
                self.inputs = [0]*self.width


#
# A simple bit machine with 8 inputs/8 outputs, and memory. 
#
class EightBitMachine(BitMachine):
        def __init__(self):
            super(EightBitMachine, self).__init__(8)
#
# Machine that takes 8 inputs and ignores 7 of them except the 
# input specified. 
#
class SingleDirectMachine(EightBitMachine):
        def __init__(self, r):
                EightBitMachine.__init__(self)
                self.mybit = r

        def output(self):
                ret = [0]*self.width
                ret[self.mybit] = self.inputs[self.mybit]
                return ret

# 
# Machine with take 8 input bits, and inverts one output based on
# what was wirtten to the specified input last.  
# All other inputs ignored.
#
class SingleInvertMachine(EightBitMachine):
        def __init__(self, r):
                EightBitMachine.__init__(self)
                self.mybit = r

        def output(self):
                ret = [0]*self.width
                ret[self.mybit] = abs(self.inputs[self.mybit]-1)
                return ret

# 
# Machine that inplements XOR between the two inputs specified on
# the output specified. All other inputs and outputs are ignored.
#
class SimpleXORMachine(EightBitMachine):
        def __init__(self, i1, i2, o):
                EightBitMachine.__init__(self)
                self.i1 = i1
                self.i2 = i2
                self.o = o
        
        def output(self):
                ret = [0]*self.width
                ret[self.o] = [1 if bool(self.inputs[self.i1]) ^ bool(self.inputs[self.i2]) else 0][0]
                return ret

#
# Machine that returns the parity of the eight-bit set on output bit 0. 
# 0 for even, 1 for odd. All other outputs are set to 0. 
#
class ParityMachine(EightBitMachine):
        def __init__(self):
                EightBitMachine.__init__(self)

        def output(self):
                ret = [0]*self.width
                ret[0] = [0 if sum(self.inputs) % 2 == 0 else 1][0]
                return ret

#
# Next several funcs generate test data.  Returns (X, Y), both 3D matricies, 
# where there first dimension is the number of instances, the 2nd is the # 
# of input or output entries, and the 3rd dimension is number entries in 
# the sequence.
# 
def gen_cmdvec_9(width=8):
        inputvec = []
        bt = random.choice(range(0, width))
        op = random.choice(['set','clear'])
        inputvec += [0]*8
        inputvec.append(1 if op == 'set' else 0)
        inputvec[bt] = 1
        return inputvec

def gen_instance(cls, seqlen, width=8):
        X = []
        Y = []
        cls.reset()
        for _ in range(seqlen):
                ivec = gen_cmdvec_9()
                X.append(ivec)
                if ivec[width] == 1:
                        cls.set_input(ivec.index(1))
                else:
                        cls.clear_input(ivec.index(1))
                #print(ivec, cls.output())
                Y.append(cls.output())
        return X, Y

def gen_dataset(cls, seqlen, number):
        Xs = []
        Ys = []
        count = 0
        while count < number:
                x, y = gen_instance(cls, seqlen)
                Xs.append(x)
                Ys.append(y)
                count += 1
        Xs = np.array(Xs, dtype=float)
        Ys = np.array(Ys, dtype=float)
        return Xs, Ys

def dataset_generator(cls, seqlen):
        while True:
                x, y = gen_instance(cls, seqlen)
                yield np.array(x), np.array(y)


##############################
# Unit tests
##############################
class TestBitMachines(unittest.TestCase):
        def setUp(self):
                self.eb = EightBitMachine()
                self.sdm = SingleDirectMachine(5)
                self.sdim = SingleInvertMachine(5)
                self.xor = SimpleXORMachine(2,5,3)
                self.par = ParityMachine()

        def test_eightbit_machine(self):
                self.assertTrue(len(self.eb.output()) == 8, "Wrong output size")
                self.assertTrue(self.eb.output() == 
                                        [0,0,0,0,0,0,0,0], "Wrong init values")
                self.eb.set_input(3)
                self.eb.set_input(4)
                self.assertTrue(self.eb.output() ==
                                        [0,0,0,1,1,0,0,0], "Set Failed")
                self.eb.clear_input(4)
                self.assertTrue(self.eb.output() ==
                                        [0,0,0,1,0,0,0,0], "Clear failed")
                self.assertTrue(self.eb.get_input(4) == 0, "Get failed")
                self.eb.reset()
                self.assertTrue(self.eb.output() == 
                                        [0,0,0,0,0,0,0,0], "Reset failed")

        def test_singledirect_machine(self):
                self.assertTrue(len(self.sdm.output()) == 8, 
                                                        "Wrong output size")
                self.assertTrue(self.sdm.output() == 
                                        [0,0,0,0,0,0,0,0], "Wrong init values")
                self.sdm.set_input(3)
                self.sdm.set_input(5)
                self.assertTrue(self.sdm.output() ==
                                        [0,0,0,0,0,1,0,0], "Set Failed")
                self.sdm.clear_input(5)
                self.assertTrue(self.sdm.output() ==
                                        [0,0,0,0,0,0,0,0], "Clear failed")
                self.assertTrue(self.sdm.get_input(3) == 1, "Get failed")
                self.sdm.reset()
                self.assertTrue(self.sdm.output() == 
                                        [0,0,0,0,0,0,0,0], "Reset failed")
                self.assertTrue(self.sdm.get_input(3) == 0, 
                                                        "Reset input failed")

        def test_singledirectinvert_machine(self):
                self.assertTrue(len(self.sdim.output()) == 8, 
                                                        "Wrong output size")
                self.assertTrue(self.sdim.output() == 
                                        [0,0,0,0,0,1,0,0], "Wrong init values")
                self.sdim.set_input(3)
                self.sdim.set_input(5)
                self.assertTrue(self.sdim.output() ==
                                        [0,0,0,0,0,0,0,0], "Set Failed")
                self.sdim.clear_input(5)
                self.assertTrue(self.sdim.output() ==
                                        [0,0,0,0,0,1,0,0], "Clear failed")
                self.assertTrue(self.sdim.get_input(3) == 1, "Get failed")
                self.sdim.reset()
                self.assertTrue(self.sdim.output() == 
                                        [0,0,0,0,0,1,0,0], "Reset failed")
                self.assertTrue(self.sdm.get_input(3) == 0, 
                                                        "Reset input failed")

        def test_xor_machine(self):
                self.assertTrue(len(self.xor.output()) == 8, 
                                                        "Wrong output size")
                self.assertTrue(self.xor.output() == 
                                        [0,0,0,0,0,0,0,0], "Wrong init values")
                self.xor.set_input(3)
                self.xor.set_input(5)
                self.assertTrue(self.xor.output() ==
                                        [0,0,0,1,0,0,0,0], "Set Failed")
                self.xor.set_input(2)
                self.assertTrue(self.xor.output() ==
                                         [0,0,0,0,0,0,0,0], "XOR Failed")
 
                self.xor.clear_input(5)
                self.assertTrue(self.xor.output() ==
                                        [0,0,0,1,0,0,0,0], "Clear failed")
                self.assertTrue(self.xor.get_input(3) == 1, "Get failed")
                self.xor.reset()
                self.assertTrue(self.xor.output() == 
                                        [0,0,0,0,0,0,0,0], "Reset failed")
                self.assertTrue(self.xor.get_input(3) == 0, 
                                                        "Reset input failed")       
        def test_par_machine(self):
                self.assertTrue(len(self.par.output()) == 8, 
                                                        "Wrong output size")
                self.assertTrue(self.par.output() == 
                                        [0,0,0,0,0,0,0,0], "Wrong init values")
                self.par.set_input(3)
                self.par.set_input(5)
                self.assertTrue(self.par.output() ==
                                        [0,0,0,0,0,0,0,0], "par 2 Failed")
                self.par.set_input(2)
                self.assertTrue(self.par.output() ==
                                         [1,0,0,0,0,0,0,0], "par 3 Failed")
 
                self.par.clear_input(5)
                self.assertTrue(self.par.output() ==
                                        [0,0,0,0,0,0,0,0], "Clear failed")
                self.assertTrue(self.par.get_input(3) == 1, "Get failed")
                self.par.reset()
                self.assertTrue(self.par.output() == 
                                        [0,0,0,0,0,0,0,0], "Reset failed")
                self.assertTrue(self.par.get_input(3) == 0, 
                                                        "Reset input failed")         
        def test_gen_dataset(self):
                x, y = gen_dataset(self.eb, 20, 2000)
                i = np.array(x)
                o = np.array(y)
                self.assertTrue(i.shape == (2000, 20, 9))
                self.assertTrue(o.shape == (2000, 20, 8))

                x,y = gen_dataset(self.xor, 20, 2000)
                i = np.array(x)
                o = np.array(y)
                self.assertTrue(i.shape == (2000, 20, 9))
                self.assertTrue(o.shape == (2000, 20, 8))

        def test_dataset_generator(self):
                it = dataset_generator(self.eb, 20)
                x,y = next(it)
                self.assertTrue(x.shape == (20, 9))
                self.assertTrue(y.shape == (20, 8))
                
        
if __name__ == '__main__':
        unittest.main()
