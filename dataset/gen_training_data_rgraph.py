#!/usr/bin/env python3
#
# generate_training_data.py: Generates an HDF5 file with randomly-generated 
#                            input and output sequences for each virtual
#                            machine type specified.  Used for training
#                            neural networks.
# 
# Copyright (c) 2018-2020 John Clemens <clemej1@umbc.edu>
#

import numpy as np
import sys
import h5py
import randgraph
import networkx as nx
from multiprocessing import Pool

if len(sys.argv) < 10:
        print("Usage: <basename.hdf5> <insize> <nstates> <outsize> <seqlen> <trainsize> <valsize> <evalsize> <machinename(s)>\n")
        sys.exit(0)

outfile = sys.argv[1]
insize = int(sys.argv[2])
nstates = int(sys.argv[3])
outsize = int(sys.argv[4])
seqlen = int(sys.argv[5])
tsize = int(sys.argv[6])
vsize = int(sys.argv[7])
ssize = int(sys.argv[8])
machlist = sys.argv[9:]

def generate_machine(machine):
        print("Opening %s for saving" % (machine+"-train-val-"+outfile,))
        out = h5py.File(machine+"-train-val-"+outfile, 'a')
        outeval = h5py.File(machine+"-eval-"+outfile, 'a')
        #outpath = h5py.File(machine+"-path-"+outfile, 'a')
        print("Generating random graph (%d, %d, %d)" % (insize, nstates, outsize))
        g = randgraph.gen_rand_graph(nstates, alphabet=range(0, insize), emitter_nodes=outsize)
        with open(machine+'-graph.graphml', 'w') as fp:
                fp.write(chr(10).join(nx.generate_graphml(g)))

        print("Generating new dataset: %s (%d,%d,%d)" % (machine, tsize, vsize, ssize))
        X, Y = randgraph.gen_graph_dataset(g, seqlen, tsize, alphabet=range(0, insize), numpy=True)
        print(X.shape, Y.shape)
        Xt = out.create_dataset('Xt', (tsize, seqlen, insize), data=X, dtype='int8', compression="gzip")
        Yt = out.create_dataset('Yt', (tsize, seqlen, outsize), data=Y, dtype='int8', compression="gzip")

        X, Y = randgraph.gen_graph_dataset(g, seqlen, vsize, alphabet=range(0, insize), numpy=True)
        Xv = out.create_dataset('Xv', (vsize, seqlen, insize), data=X, dtype='int8', compression="gzip")
        Yv = out.create_dataset('Yv', (vsize, seqlen, outsize), data=Y, dtype='int8', compression="gzip")

        X, Y = randgraph.gen_graph_dataset(g, seqlen, ssize, alphabet=range(0, insize), numpy=True)
        Xs = outeval.create_dataset('Xs', (ssize, seqlen, insize), data=X, dtype='int8', compression="gzip")
        Ys = outeval.create_dataset('Ys', (ssize, seqlen, outsize), data=Y, dtype='int8', compression="gzip")

        # Generate a training sample to get output/input size:
        #test_x, test_y = next(mach_types[machine][1](mach_types[machine][0], seqlen, key=[('set',1),('clear',0),('set',6)]))
        #print(test_x.shape, test_y.shape)

        #Xt = out.create_dataset('Xt', (tsize, seqlen, test_x.shape[1]), data=train_x, dtype='int8', compression="gzip")
        #Yt = out.create_dataset('Yt', (tsize, seqlen, test_y.shape[1]), data=train_y, dtype='int8', compression="gzip")
        #Xv = out.create_dataset('Xv', (vsize, seqlen, test_x.shape[1]), data=val_x, dtype='int8', compression="gzip")
        #Yv = out.create_dataset('Yv', (vsize, seqlen, test_y.shape[1]), data=val_y, dtype='int8', compression="gzip")
        #Xs = outeval.create_dataset('Xs', (ssize, seqlen, test_x.shape[1]), data=eval_x, dtype='int8', compression="gzip")
        #Ys = outeval.create_dataset('Ys', (ssize, seqlen, test_y.shape[1]), data=eval_y, dtype='int8', compression="gzip")
        #Xp = outpath.create_dataset('Xs', (ssize, seqlen, test_x.shape[1]), dtype='int8', compression="gzip")
        #Yp = outpath.create_dataset('Ys', (ssize, seqlen, test_y.shape[1]), dtype='int8', compression="gzip")
      
        #def fill_dataset(ds_x, ds_y, machine, seqlen, size, key=[]):
        #        it = mach_types[machine][1](mach_types[machine][0], seqlen)#, key=key)
        #        for idx in range(size):
        #                if (idx % 128) == 0:
        #                        print(idx)
        #                x, y = next(it)
        #                ds_x[idx] = x.astype(np.int8)
        #                ds_y[idx] = y.astype(np.int8)

        #print(" - Training..")
        #fill_dataset(Xt, Yt, machine, seqlen, tsize, key=[('set',1),('clear',0),('set',6)])
        #print(" - Validation..")
        #fill_dataset(Xv, Yv, machine, seqlen, vsize, key=[('set',1),('clear',0),('set',6)])
        #print(" - Eval..")
        #fill_dataset(Xs, Ys, machine, seqlen, ssize, key=[('set',1),('clear',0),('set',6)])
        #print(" - Pathological ..")
        #fill_dataset(Xp, Yp, machine, seqlen, ssize, key=[('set',1),('clear',0),('set',6)])

        out.close()
        outeval.close()
        print("Done "+machine)
        #outpath.close()

if __name__ == '__main__':
        with Pool(8) as p:
                p.map(generate_machine, machlist)
        
