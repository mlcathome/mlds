#!/usr/bin/env python3
#
# Utility functions to build random graph datasets. 
# 
# Copyright (c) 2018 John Clemens <clemej1@umbc.edu>
# 

import networkx as nx
import numpy as np
import random
import pygraphviz
from networkx.drawing.nx_agraph import write_dot

#
# Generate a random graph using networkx, that approximates a DFA.  
# This graph contains at least one hamilton cycle, and each node has N
# outgoing edges, which may loop back to iself. This guarantees that
# every input, something will happen (i.e., there are no invalid
# inputs). 
#
def gen_rand_graph(N, alphabet=['a','b'], emitter_nodes=8, cycle=True):
    # create empty graph. Needs parallel edges (MultiDiGraph)
    graph = nx.MultiDiGraph()

    # Shuffle the list ints representing nodes.
    nl = list(range(0,N))
    random.shuffle(nl)
    
    # choose and label emitter nodes. 
    ens = random.sample([x for x in range(0,N) if x != nl[0]], emitter_nodes-1)

    # Add nodes to the graph and label them. add extra labels for start and 
    # end nodes. First node always an emitter. 
    emitter_count = 0
    graph.add_node(nl[0], label=nl[0], start=True, emitter=True, emit_output=emitter_count)
    emitter_count += 1
    for n in nl[1:]:
        if n in ens:
            graph.add_node(n, label=n, emitter=True, emit_output=emitter_count)
            emitter_count += 1
        else:
            graph.add_node(n, label=n, emitter=False)
    #graph.add_node(nl[-1], label=nl[-1], emitter=nl[-1] in ens, emit_output=emitter_count) # XXXjc unused: end=True)

    # Add edges for cycle.
    for e in zip(nl, nl[1:]):
        graph.add_edge(e[0], e[1], label=random.choice(alphabet))
    if cycle:
        graph.add_edge(nl[-1], nl[0], label=random.choice(alphabet))

    # Add random edges for the label not used in cycle.
    for e in list(graph.out_edges(nl, data=True)):
        for newlabel in [x for x in alphabet if x != e[2]['label']]:
            graph.add_edge(e[0], random.choice([x for x in nl]), label=newlabel)
        
    return graph



def graph_to_dot(g, fn):
    with open(fn, "w") as f:
        f.write("digraph {\n")
        for e in g.edges(data=True):
            f.write('    %d -> %d [label = "%s"];\n' % (e[0],e[1],e[2]['label']))
        for n in g.nodes(data=True):
            if n[1].get('start', None) != None:
                f.write("    %d [label=\"%d:%d\", fillcolor = \"green\" shape = \"doubleoctagon\" style=\"filled\"];\n" % (n[0],n[0],n[1]['emit_output'],))
            elif n[1].get('emitter', None) == True:
                f.write("    %d [label=\"%d:%d\", fillcolor = \"yellow\" style=\"filled\"];\n" % (n[0],n[0],n[1]['emit_output']))
        f.write("}\n")

#
# Generate a random sequence of commands N entries long of alphabet.
#
def gen_cmds(N, alphabet=['a','b']):
    return [random.choice(alphabet) for _ in range(0,N)]

#
# Given a networkx digraph network with edges labelled as the
# alphabet and the inital node labelled "start = True", then
# traverse the graph given the sequence of commands, and output
# the ordered traversal.
#
def run_network(g, seq, include_init=False, init_val=0):
    ret = []

    start = -1
    labels = {}
    for n in g.nodes(data=True):
        if n[1].get('start', None):
            start=n[0]
            start_val = n[1]['emit_output']
        labels[n[0]] = n[1]
    if start == -1:
        print("Couldn't find start node")
        return None

  
    cur_node = start
    cur_val = start_val
    if include_init:
        ret.append(start[0] if labels[start]['emitter'] else init_val) 
        
    for cmd in seq:
        #print(cmd, cur_node)
        tmp = [e[1] for e in g.out_edges([cur_node], data=True)
                                             if e[2]['label'] == cmd]
        #print(tmp)
        cur_node = tmp[0]
        if labels[cur_node]['emitter']:
            ret.append(labels[cur_node]['emit_output'])
            cur_val = labels[cur_node]['emit_output']
        else:
            ret.append(cur_val)

    return ret


#
# Convert command string to numpy input vector:
# input encoding: AxS entry, where A is one-hot encoded from the
# index in alphabet.
# 
def cmd_to_vec(seq, alphabet=['a', 'b'], numpy=False):
    ret = []
    for x in seq:
        r = [0.0]*len(alphabet)
        r[alphabet.index(x)] = 1.0
        ret.append(r)
    if numpy:
        return np.array(ret, dtype='int8')
    else:
        return ret

# 
# convert output from run_network into a numpy output vector
# Output encoding: One-hot up to 20(?) nodes. NxN. 
#
def output_to_vec(output, N=20, numpy=False):
    ret = []
    for o in output:
        row = [0.0]*N
        assert(o != -1)
        row[o] = 1.0
        ret.append(row)
    if numpy:
        return np.array(ret, dtype='int8')
    else:
        return ret

# 
# Given a graph, generate a random input sequence and run the
# graph to generate the numpy output sequences.
#
def gen_graph_dataset(graph, seqlen, ds_size, alphabet=['a','b'], numpy=True):
    progs = [gen_cmds(seqlen, alphabet=alphabet) for x in range(ds_size)]
    X = [cmd_to_vec(x, alphabet=alphabet, numpy=numpy) for x in progs]
    
    outsize = 0
    for n in graph.nodes(data=True):
        if n[1]['emitter']:
            outsize += 1
    #print(progs[0])
    outs = [run_network(graph, x) for x in progs]
    #print(outs[0])
    Y = [output_to_vec(x,N=outsize,numpy=numpy) for x in outs]
    
    if numpy:
        return np.array(X, dtype='int8'), np.array(Y, dtype='int8')
    else:
        return X, Y

#
# 
#
def gen_dataset(n_graphs, g_size, outsize, seqlen, n_train, n_val, n_test, alphabet=['a','b'], numpy=True):

    ret = []

    graphs = [gen_rand_graph(g_size, alphabet=alphabet, emitter_nodes=outsize) for _ in range(0, n_graphs)]
    #adjmats = [graph_to_adjmat(g) for g in graphs]

    for _, g in enumerate(graphs):
        row = { 'graphml': chr(10).join(nx.generate_graphml(g)),
                'seqlen': seqlen,
                'size': g_size,
                'models': [] }

        row['train'] = gen_graph_dataset(g, seqlen, n_train, alphabet=alphabet, numpy=numpy)
        row['verify'] = gen_graph_dataset(g, seqlen, n_val, alphabet=alphabet, numpy=numpy)
        row['test'] = gen_graph_dataset(g, seqlen, n_test, alphabet=alphabet, numpy=numpy)

        ret.append(row)

    return ret

# 
# worker for parallel dataset generation
# Takes graph size and sequence len and number of test/val/train
#     Generates one graph
#     generates the train / validation / test samples for that graph
#     returns (adjmat, seqlen, train, val, test)
#
def pgen_dataset_worker(g_size, outsize, seqlen, n_train, n_val, n_test, alphabet):
    g = gen_rand_graph(g_size, emitter_nodes=outsize, alphabet=alphabet)
    train = gen_graph_dataset(g, seqlen, n_train, alphabet=alphabet)
    val = gen_graph_dataset(g, seqlen, n_val, alphabet=alphabet)
    test = gen_graph_dataset(g, seqlen, n_test, alphabet=alphabet)

    return (chr(10).join(nx.generate_graphml(g)), train, val, test)

def pgen_dataset(n_graphs, g_size, outsize, seqlen, n_train, n_val, n_test, alphabet=['a','b']):
    import multiprocessing

    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        awork = [pool.apply_async(pgen_dataset_worker, 
                            (g_size, outsize, seqlen, n_train, n_val, n_test, alphabet)) for _
                                                in range(n_graphs)]
        print("Waiting for results...")
        [x.wait() for x in awork]
        print("Collecting results...")
        res = [x.get() for x in awork]

    return res

if __name__ == '__main__':
    import unittest
    g = gen_rand_graph(16, alphabet=['a','b','c','d'], emitter_nodes=14)
    print(gen_graph_dataset(g, 10, 1, alphabet=['a','b','c','d']))
    print(graph_to_dot(g, '/tmp/out.dot'))
