#!/usr/bin/env python3
#
# generate_training_data.py: Generates an HDF5 file with randomly-generated 
#                            input and output sequences for each virtual
#                            machine type specified.  Used for training
#                            neural networks.
# 
# Copyright (c) 2018-2020 John Clemens <clemej1@umbc.edu>
#

import numpy as np
import sys
import h5py
import bitmachines_modified as bm
from multiprocessing import Pool

if len(sys.argv) < 7:
        print("Usage: <basename.hdf5> <seqlen> <trainsize> <valsize> <showsize> <machinename(s)>\n")
        sys.exit(0)

outfile = sys.argv[1]
seqlen = int(sys.argv[2])
tsize = int(sys.argv[3])
vsize = int(sys.argv[4])
ssize = int(sys.argv[5])
machlist = sys.argv[6:]

mach_types = {
    'EightBitModified': (bm.EightBitModified(), bm.dataset_generator),
    'SingleDirectModified': (bm.SingleDirectModified(6), bm.dataset_generator),
    'SingleInvertModified': (bm.SingleInvertModified(3), bm.dataset_generator),
    'SimpleXORModified': (bm.SimpleXORModified(3,7,5), bm.dataset_generator),
    'ParityModified': (bm.ParityModified(), bm.dataset_generator),
}

def generate_machine(machine):
        print("Opening %s for saving" % (machine+"-train-val-"+outfile,))
        out = h5py.File(machine+"-train-val-"+outfile, 'a')
        outeval = h5py.File(machine+"-eval-"+outfile, 'a')
        outpath = h5py.File(machine+"-path-"+outfile, 'a')
        print("Generating new dataset: %s (%d,%d,%d)" % (machine, tsize, vsize, ssize))

        # Generate a training sample to get output/input size:
        test_x, test_y = next(mach_types[machine][1](mach_types[machine][0], seqlen, key=[('set',1),('clear',0),('set',6)]))
        print(test_x.shape, test_y.shape)

        Xt = out.create_dataset('Xt', (tsize, seqlen, test_x.shape[1]), dtype='float32', compression="gzip")
        Yt = out.create_dataset('Yt', (tsize, seqlen, test_y.shape[1]), dtype='float32', compression="gzip")
        Xv = out.create_dataset('Xv', (vsize, seqlen, test_x.shape[1]), dtype='float32', compression="gzip")
        Yv = out.create_dataset('Yv', (vsize, seqlen, test_y.shape[1]), dtype='float32', compression="gzip")
        Xs = outeval.create_dataset('Xs', (ssize, seqlen, test_x.shape[1]), dtype='float32', compression="gzip")
        Ys = outeval.create_dataset('Ys', (ssize, seqlen, test_y.shape[1]), dtype='float32', compression="gzip")
        Xp = outpath.create_dataset('Xs', (ssize, seqlen, test_x.shape[1]), dtype='float32', compression="gzip")
        Yp = outpath.create_dataset('Ys', (ssize, seqlen, test_y.shape[1]), dtype='float32', compression="gzip")
      
        def fill_dataset(ds_x, ds_y, machine, seqlen, size, key=[]):
                it = mach_types[machine][1](mach_types[machine][0], seqlen, key=key)
                for idx in range(size):
                        if (idx % 128) == 0:
                                print(idx)
                        x, y = next(it)
                        ds_x[idx] = x.astype(np.float32)
                        ds_y[idx] = y.astype(np.float32)

        print(" - Training..")
        fill_dataset(Xt, Yt, machine, seqlen, tsize, key=[('set',1),('clear',0),('set',6)])
        print(" - Validation..")
        fill_dataset(Xv, Yv, machine, seqlen, vsize, key=[('set',1),('clear',0),('set',6)])
        print(" - Eval..")
        fill_dataset(Xs, Ys, machine, seqlen, ssize, key=[('set',1),('clear',0),('set',6)])
        print(" - Pathological ..")
        fill_dataset(Xp, Yp, machine, seqlen, ssize, key=[('set',1),('clear',0),('set',6)])

        out.close()
        outeval.close()
        outpath.close()

if __name__ == '__main__':
        with Pool(len(machlist)) as p:
                p.map(generate_machine, machlist)
        
