# Machine Learning Dataset Generator (mlds)

This application is part of the [MLC@Home](https://www.mlcathome.org/) project.

[![pipeline status](https://gitlab.com/clemej/mlds/badges/master/pipeline.svg)](https://gitlab.com/clemej/mlds/-/commits/master)

MLDS creates and trains neural networks on various training data using 
PyTorch.  At the moment, it includes support for

* RNN: stacked GRU/LSTM networks of various sizes
* Training/Validation Datasets in 3D HDF5 files for sequence learning problems (float32 or int8)

For more information please see https://www.mlcathome.org/

## Supported Environments

MLDS supports running in Windows, Linux on amd64, arm64, and arm32.  Porting to other architectures
and UNIX-like OSs should be straightforward.

## Compiling

MLDS is a C++ application that uses CMake and git submodules for some of its dependencies. Where
possible, the program is statically linked to run on the widest range of systems as possible. 

### Linux

Compiling on Linux is as all the pre-requisites are available in most 
repositories.  MLDS itself has three main dependencies:

* PyTorch (libTorch/C++ frontend)
* BOINC (libraries)
* libhdf5 (C++/HL frontend)

All three are included as git submodules.  The easiest way to build would be:

1. Install a minimal debian environment (containers work fine)
2. Install the following necessary dependencies:

    build-essential gfortran git cmake python3 python3-typing-extensions python3-yaml
    ccache libnuma-dev libmpfr-dev libgmp-dev libfftw3-dev m4 pkg-config autoconf automake
    libtool libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev libxmu-dev libxi-dev libjpeg-dev

3. `git clone http://gitlab.com/mlcathome/mlds`
4. `cd mlds`
5. `git submodule update --init --recursive`
6. `mkdir build && cd build`
7. `cmake -DINTERNAL_PYTORCH=ON -DINTERNAL_OPENBLAS=ON -DGPU=OFF -DUSE_CUDA=OFF -DUSE_ROCM=OFF ..`
8. `make -j8 mlds`

Compiling takes a long time, as PyTorch in particular takes a very long time. When complete, there
will me a `mlds` binary in the build directory. 

### Windows

Windows builds require more manual setup. There's a lot more detail than I will go into
here, but in general you'll need to 

* Download and install Visual Studio (2019 Community edition tested)
* Download and install vcpkg
* Use vcpkg to install a copy of libhdf5 for x64-windows-static
* Download the BOINC repo
* Compile libboinc,libboincapi, and libgraphics2 as static libraries
* Download the windows pre-compiled libtorch DLLs from pytorch.org
* unzip them to a known location

Then you can 

* git clone https://gitlab.com/clemej/mlds
* cd mlds
* hand modify CMakeLists.txt to change the hard-coded paths to point to where the above are
  located on your system
* mkdir build
* cd build
* cmake .. 
* cmake --build . --config Release --target mlds

After that you should have a working `mlds.exe` binary in Release/ .

## Testing

Testing requires an exmaple dataset. Once you have one, you can copy it to the same
directory as the mlds binary (make sure it is called `dataset.hdf5`), and run 

    ./mlds

If all goes well, you should see output like:

```
Re-exec()-ing to set number of threads correctly...
Machine Learning Dataset Generator v9.50 (Linux/aarch64) (libTorch: release/1.6)
[2020-08-20 00:44:36	                main:343]	:	INFO	:	Set logging level to 1
[2020-08-20 00:44:36	                main:351]	:	INFO	:	Running in BOINC Standalone mode
[2020-08-20 00:44:36	                main:356]	:	INFO	:	Resolving all filenames
[2020-08-20 00:44:36	                main:364]	:	INFO	:	Resolved: dataset.hdf5 => dataset.hdf5 (exists = 1)
[2020-08-20 00:44:36	                main:364]	:	INFO	:	Resolved: model.cfg => model.cfg (exists = 1)
[2020-08-20 00:44:36	                main:364]	:	INFO	:	Resolved: model-final.pt => model-final.pt (exists = 0)
[2020-08-20 00:44:36	                main:364]	:	INFO	:	Resolved: model-input.pt => model-input.pt (exists = 0)
[2020-08-20 00:44:36	                main:364]	:	INFO	:	Resolved: snapshot.pt => snapshot.pt (exists = 0)
[2020-08-20 00:44:36	                main:378]	:	INFO	:	Dataset filename: dataset.hdf5
[2020-08-20 00:44:36	                main:380]	:	INFO	:	Configuration: 
[2020-08-20 00:44:36	                main:381]	:	INFO	:	    Validation Loss Threshold: 0.0001
[2020-08-20 00:44:36	                main:382]	:	INFO	:	    Max Epochs: 100
[2020-08-20 00:44:36	                main:383]	:	INFO	:	    Batch Size: 128
[2020-08-20 00:44:36	                main:384]	:	INFO	:	    Patience: 10
[2020-08-20 00:44:36	                main:385]	:	INFO	:	    Hidden Width: 12
[2020-08-20 00:44:36	                main:386]	:	INFO	:	    # Recurrent Layers: 4
[2020-08-20 00:44:36	                main:387]	:	INFO	:	    # Backend Layers: 4
[2020-08-20 00:44:36	                main:389]	:	INFO	:	Preparing Dataset
[2020-08-20 00:44:36	load_hdf5_ds_into_tensor:28]	:	INFO	:	Loading Dataset /Xt from dataset.hdf5 into memory
[2020-08-20 00:44:37	load_hdf5_ds_into_tensor:28]	:	INFO	:	Loading Dataset /Yt from dataset.hdf5 into memory
[2020-08-20 00:44:38	                load:103]	:	INFO	:	Successfully loaded dataset of 2048 examples into memory.
[2020-08-20 00:44:38	load_hdf5_ds_into_tensor:28]	:	INFO	:	Loading Dataset /Xv from dataset.hdf5 into memory
[2020-08-20 00:44:38	load_hdf5_ds_into_tensor:28]	:	INFO	:	Loading Dataset /Yv from dataset.hdf5 into memory
[2020-08-20 00:44:39	                load:103]	:	INFO	:	Successfully loaded dataset of 512 examples into memory.
[2020-08-20 00:44:39	                main:395]	:	INFO	:	Creating Model
[2020-08-20 00:44:39	                main:400]	:	INFO	:	Preparing config file
[2020-08-20 00:44:39	                main:412]	:	INFO	:	Creating new config file
[2020-08-20 00:44:39	                main:443]	:	INFO	:	Loading DataLoader into Memory
[2020-08-20 00:44:39	                main:446]	:	INFO	:	Starting Training
[2020-08-20 00:46:22	                main:458]	:	INFO	:	Epoch 1 | loss: 0.0364953 | val_loss: 0.0315741 | Time: 103431 ms
[2020-08-20 00:48:05	                main:458]	:	INFO	:	Epoch 2 | loss: 0.030897 | val_loss: 0.0290543 | Time: 102836 ms
[2020-08-20 00:49:48	                main:458]	:	INFO	:	Epoch 3 | loss: 0.0265094 | val_loss: 0.0224487 | Time: 103135 ms
...
```
## Changelog
v9.8x Oct 2020
* CUDA support
* ROCM support
* DS3 support
* Lots of other things

v9.50 Aug 20th 2020

* Updated README.md
* Upped version to match the boinc server's weird three-digit version
* Added support for datasets in INT8 format as well as Float32 (helps memory usage)
* ARM64/ARM32 support
* Capture libtorch and arch information
* Lots on internal improvements, including CI support

v9.20 

* Stable release for Datasets 1 and 2
* Windows support!

## License

GPLv3, with the exception of src/json.hpp, which is MIT. 

