#include <torch/torch.h>
#include <iostream>
#include <string>
#include <typeinfo>
#include <H5Cpp.h>

class CustomDataset : public torch::data::datasets::Dataset<CustomDataset> {
	using Example = torch::data::Example<torch::Tensor, torch::Tensor>;

	torch::Tensor X, Y;

	public:
		CustomDataset(const torch::Tensor Xs, const torch::Tensor Ys) : X(Xs), Y(Ys) {}
		Example get(size_t index) {
			return {X[index], Y[index]};
		}
		torch::optional<size_t> size() const {
    			return X.size(0);
  		}
};

torch::Tensor load_hdf5_ds_into_tensor(std::string filename, std::string dataset)
{
	H5::H5File h5f(filename, H5F_ACC_RDONLY);
	H5::DataSet dset = h5f.openDataSet(dataset);
	H5T_class_t type_class = dset.getTypeClass();
	H5::DataSpace dspace = dset.getSpace();

	//std::cout << type_class << std::endl;
	H5::FloatType ftype = dset.getFloatType();
	size_t size = ftype.getSize();
	//std::cout << size << std::endl;


	int ndim = dspace.getSimpleExtentNdims();
	hsize_t dims[ndim];
	dspace.getSimpleExtentDims(dims, NULL);
        //std::cout << "rank " << ndim << ", dimensions " <<
        //      (unsigned long)(dims[0]) << " x " <<
        //      (unsigned long)(dims[1]) << " x " <<
	//      (unsigned long)(dims[2]) << std::endl;
	//double data[dims[0]][dims[1]][dims[2]];
	float *data = static_cast<float *>(malloc(dims[0]*dims[1]*dims[2]*sizeof(float)));
	if (!data) {
		std::cerr << "Error allocating memory for dataset\n";
		return torch::randn({1,1,1});
	}
	H5::DataSpace memspace( 3, dims );
	dset.read(data, H5::PredType::NATIVE_FLOAT, memspace, dspace);
	auto ret = torch::from_blob(data, {dims[0], dims[1], dims[2]}, torch::kFloat32);
	//std::cout << ret[0][0] << "\n" << ret[0][1] << "\n";
	//std::cout << ret[42][0] << "\n" << ret[42][1] << "\n";
	
	return ret;
}

CustomDataset build_tensor_dataset(std::string filename, std::string datastr, std::string labels)
{
	torch::Tensor X = load_hdf5_ds_into_tensor(filename, datastr);
	torch::Tensor Y = load_hdf5_ds_into_tensor(filename, labels);
	return CustomDataset(std::move(X), std::move(Y));
}

/*
 * Create a model that can be trained on each of the
 * bitmachine models. 9 input pins, with memory, and
 * eight outputs. Note for some machines, only
 * a single output is active.
 */
struct BitMachineModel : torch::nn::Module {
	BitMachineModel(int input_size, int output_size,
			int hidden_width = 12, 
			int recursive_layers = 4, 
			int backend_layers = 4) {

		recurrent = register_module("recurrent", 
				torch::nn::GRU(
					torch::nn::GRUOptions(input_size,hidden_width)
						.num_layers(recursive_layers)
						.batch_first(true)));
		l1 = register_module("l1", torch::nn::Linear(hidden_width, hidden_width));
		l2 = register_module("l2", torch::nn::Linear(hidden_width, hidden_width));
		l3 = register_module("l3", torch::nn::Linear(hidden_width, hidden_width));
		l4 = register_module("l4", torch::nn::Linear(hidden_width, hidden_width));
		output = register_module("output", 
					torch::nn::Linear(hidden_width, output_size));
		//init_hidden = torch::randn({recursive_layers,input_size
	}

	torch::Tensor forward(torch::Tensor x) {
		//std::cout << "FWD: " << x.size(0) << "," << x.size(1) << "," << x.size(2) << "\n";
		//std::tie(x, hidden) = recurrent->forward(x);
		//std::cout << hidden.size(0) << "," << hidden.size(1) << "," << hidden.size(2) << "\n";
		//std::cout << x.size(0) << "," << x.size(1) << "," << x.size(2) << "\n";
		auto out = std::get<0>(recurrent->forward(x));//.index({torch::indexing::Slice(), -1});
		out = l1->forward(out);
		out = l2->forward(out);
		out = l3->forward(out);
		out = l4->forward(out);

		out = output->forward(out);
		return out;
	}

	/* 
	 * See https://pytorch.org/tutorials/advanced/cpp_frontend.html
	 * for why nullptr.
	 */
	torch::Tensor		init_hidden;
	torch::nn::GRU		recurrent{nullptr};
	torch::nn::Linear 	l1{nullptr};
	torch::nn::Linear 	l2{nullptr};
	torch::nn::Linear 	l3{nullptr};
	torch::nn::Linear 	l4{nullptr};
	torch::nn::Linear 	output{nullptr};
};

int main() {
	auto model = std::make_shared<BitMachineModel>(9, 8);

	//std::cout << model << "\n";
	//torch::Tensor i = torch::rand({1, 32, 9});
	//std::cout << i << "\n";
	//std::cout << i.size(0) << " " << i.size(1) << " " << i.size(2) << "\n";
	//std::cout << model->forward(i) << "\n";

	std::cout << "Loading Dataset into memory\n";

	CustomDataset dset = build_tensor_dataset(
					"dataset.hdf5",
					"EightBitMachine/Xt",
					"EightBitMachine/Yt");
	CustomDataset vset = build_tensor_dataset(
					"dataset.hdf5",
					"EightBitMachine/Xv",
					"EightBitMachine/Yv");

	auto train_loader = torch::data::make_data_loader(dset.map(
				torch::data::transforms::Stack<>()), 64);
	auto val_loader = torch::data::make_data_loader(vset.map(
				torch::data::transforms::Stack<>()), 64);

	std::cout << "Done..\n";
	torch::optim::Adam optimizer(model->parameters(), 0.01);
	double val_loss = 999999.0;
	double train_loss = 999999.0;
	int total_epochs = 0;
	std::cout << "Starting Training..." << std::endl;
  	for (size_t epoch = 1; epoch <= 1024, val_loss > 0.0001; ++epoch) {
		model->train();
		++total_epochs;
		std::cout << "Epoch " << epoch << std::endl;
		auto t_start = std::chrono::high_resolution_clock::now();
    		size_t batch_index = 0;
		double running_loss = 0.0;
    		// Iterate the data loader to yield batches from the dataset.
    		for (auto& batch : *train_loader) {
			//std::cout << " after loader: " << batch[0].data[0] << " " << batch[0].target[0]<< '\n';
			std::vector<torch::Tensor> Xs_list, Ys_list;
		        std::transform(batch.begin(), batch.end(), std::back_inserter(Xs_list), [](auto &e) {return e.data;});
		        std::transform(batch.begin(), batch.end(), std::back_inserter(Ys_list), [](auto &e) {return e.target;});
			//std::cout << " after mapfunc: " << Xs_list[0][0] << " " << Ys_list[0][0]<< '\n';
			torch::Tensor X = torch::stack(Xs_list);
			torch::Tensor Y = torch::stack(Ys_list);
			std::cout << X.size(0) << "," << X.size(1) << "," << X.size(2) << X[0][0] << "\n";
			std::cout << Y.size(0) << "," << Y.size(1) << "," << Y.size(2) << Y[0][0] << "\n";

			// Convert batch to tensor
			//torch::Tensor bdata = torch::from_blob(batch[0]);
			//torch::Tensor blabel = torch::from_blob(batch.target);
      			// Execute the model on the input data.
      			auto prediction = model->forward(X);//batch.data);
			//std::cout << prediction.size(0) << "," << prediction.size(1) << "," << prediction.size(2) << prediction << "\n";
			//std::cout << Y.size(0) << "," << Y.size(1) << "," << Y.size(2) << Y << "\n";			
      			// Compute a loss value to judge the prediction of our model.

			auto loss = torch::mse_loss(prediction, Y);//batch.target);
			running_loss += loss.item<double>() * X.size(0);

			// Reset gradients.
      			optimizer.zero_grad();
      			// Compute gradients of the loss w.r.t. the parameters of our model.
      			loss.backward();
      			// Update the parameters based on the calculated gradients.
      			optimizer.step();
      			// Output the loss and checkpoint every 16 batches.
      			if (++batch_index % 16 == 0) {
        			std::cout << "Epoch: " << epoch << " | Batch: " << batch_index
                  		<< " | Loss: " << running_loss / batch_index*X.size(0) << std::endl;
        			// Serialize your model periodically as a checkpoint.
       	 			torch::save(model, "model.pt");
      			}
		}
		auto t_end = std::chrono::high_resolution_clock::now();
		train_loss = running_loss / static_cast<double>(dset.size().value());		
		std::cout << "Epoch " << epoch << " : Training loss : " << train_loss << 
			" : Time : " << std::chrono::duration<double, std::milli>(t_end-t_start).count() << " ms" << std::endl;
		std::cout << "Validating..." << std::endl;
		model->eval();
		running_loss = 0.0;
		batch_index = 0;
		for (auto& batch : *val_loader) {
			torch::NoGradGuard ngg;

			std::vector<torch::Tensor> Xs_list, Ys_list;
		        std::transform(batch.begin(), batch.end(), std::back_inserter(Xs_list), [](auto &e) {return e.data;});
		        std::transform(batch.begin(), batch.end(), std::back_inserter(Ys_list), [](auto &e) {return e.target;});
			torch::Tensor X = torch::stack(Xs_list);
			torch::Tensor Y = torch::stack(Ys_list);
      			// Execute the model on the input data.
      			auto prediction = model->forward(X);
			//std::cout << prediction.size(0) << "," << prediction.size(1) << "," << prediction.size(2) << prediction << "\n";
			//std::cout << Y.size(0) << "," << Y.size(1) << "," << Y.size(2) << Y << "\n";			
      			// Compute a loss value to judge the prediction of our model.

			auto loss = torch::mse_loss(prediction, Y.detach());
			running_loss += loss.item<double>() * X.size(0);

      			// Output the loss and checkpoint every 16 batches.
      			if (++batch_index % 16 == 0) {
        			std::cout << "Epoch: " << epoch << " | Batch: " << batch_index
                  		<< " | Val Loss: " << running_loss / batch_index*X.size(0) << std::endl;
      			}
		}
		val_loss = running_loss / static_cast<double>(vset.size().value());
		std::cout << "Epoch " << epoch << " loss: " << train_loss << " val_loss: " << val_loss << std::endl; 
		torch::save(model, "model.pt");
  	}
	std::cout << "Exiting. Epochs: " << total_epochs << " loss: " << train_loss << " val_loss: " << val_loss << std::endl; 
	std::cout << "Saving final model...";
	torch::save(model, "model-final.pt");
	std::cout << "done. " << std::endl;

	return 0;
}
