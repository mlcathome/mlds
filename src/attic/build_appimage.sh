#!/bin/bash

LINUXDEPLOY=~/.local/bin/linuxdeploy-x86_64.AppImage

${LINUXDEPLOY} -e build/mlds -i pkg/mlds.png -l /usr/lib/x86_64-linux-gnu/libstdc++.so.6 --create-desktop-file --output appimage --appdir tmp.appimage && rm -rf tmp.appimage && mv mlds-*.AppImage mlds.AppImage
