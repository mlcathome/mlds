#include <iostream>
#ifndef __STATUS_H__
#define __STATUS_H__
/*
 * Store the current status in a json document for easy saving
 * for snapshots and for returning to the user. 
 */
class MetaStatus {
        
        public:
                MetaStatus(std::string cfg_filename);

                void append_train_hist(double loss) {
                        train_hist.push_back(loss);
                };
                void append_val_hist(double loss) {
                        val_hist.push_back(loss);
                };
                unsigned int completed_epochs(void) {
                        return train_hist.size();
                };

                int save(void);
                int load(void);
                int exists(void);
                

                /* I could make these all private, but I like to annoy language purists. */
                std::string dataset_filename;
                std::string version;
                std::string model_type = "GRU";
	        unsigned int batch_size = 128;
	        double val_loss_threshold = 0.0001;
                double learning_rate = 0.01;
                unsigned int max_epochs = 128;
	        unsigned int hidden_width = 12;
	        unsigned int rhidden = 4;
	        unsigned int bhidden = 4;
	        unsigned int patience = 10;
                unsigned int input_width = 0;
                unsigned int output_width = 0;
                unsigned int nthreads = 1;
                double dropout_pct = 0.0;
                bool gpu = false;

        private:
                std::string config_filename;
                std::vector<double> train_hist;
                std::vector<double> val_hist;

};
#endif /* __STATUS_H__ */
