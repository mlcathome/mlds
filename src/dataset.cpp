#include <errno.h>

#include <torch/torch.h>
#include <H5Cpp.h>

#include <llog.h>
#include <dataset.h>

MLDSDataset::MLDSDataset(const std::string &filename, 
		const std::string &xs_str, const std::string &ys_str, int ndims) :
			dataset_filename(filename), xs_name(xs_str), ys_name(ys_str),
                        _x_width(0), _y_width(0), ndims(ndims)
{
	return;
}

torch::Tensor MLDSDataset::load_hdf5_ds_into_tensor(const std::string &dataset)
{
	H5::H5File h5f(dataset_filename, H5F_ACC_RDONLY);
	H5::DataSet dset = h5f.openDataSet(dataset);
	H5T_class_t type_class = dset.getTypeClass();
	H5::DataSpace dspace = dset.getSpace();

        /*
         * Dataset is wither in8 or float32 at the moment. 
         */

	INFO << "Loading Dataset " << dataset << " from " << dataset_filename << " into memory";
	DEBUG << "- Data Type : " << type_class;
        if (type_class == H5T_INTEGER) {
                int_type = true; 
                DEBUG << "- Integer type";
	        H5::IntType ftype = dset.getIntType();
	        size_t tsize = ftype.getSize();
	        DEBUG << "- Data Type Size : " << tsize;
        } else {
                DEBUG << "- Float type";
	        H5::FloatType ftype = dset.getFloatType();
	        size_t tsize = ftype.getSize();
	        DEBUG << "- Data Type Size : " << tsize;
        } 

                
	// Can't use the below to allocate array with dynamic size. VC2019 issue. XXXjc dspace.getSimpleExtentNdims();
	//int ndim = dspace.getSimpleExtentNdims();
	hsize_t dims[3]; // Assume we won't have more than 3d data at the moment (
	dspace.getSimpleExtentDims(dims, NULL);
        DEBUG << "- Rank " << ndims << ", dimensions: ";
	uint64_t nentries = 1;
	for(int i = 0; i < ndims; i++) {
              DEBUG << " --- " << (unsigned long)(dims[i]);
	      nentries *= dims[i];
	}
        int8_t *int_data = NULL;
        float *float_data = NULL;
        if (int_type) {
 	        DEBUG << "- Loading data as 8-bit int: " << nentries*sizeof(int8_t) << " bytes.";
	        int_data = static_cast<int8_t *>(malloc(nentries*sizeof(int8_t)));
        } else {
	        DEBUG << "- Loading data as 32-bit float: " << nentries*sizeof(float) << " bytes.";
	        float_data = static_cast<float *>(malloc(nentries*sizeof(float)));
        }
	if (float_data == NULL && int_data == NULL) {
	        LERROR << "- Error allocating memory for dataset";
		errflag = true;
		return torch::zeros({1});
	}
	H5::DataSpace memspace( ndims, dims );
        if (int_type) {
                dset.read(int_data, H5::PredType::STD_I8LE, memspace, dspace);
        } else {
        	dset.read(float_data, H5::PredType::NATIVE_FLOAT, memspace, dspace);
        }
	DEBUG << "- Dataset loaded into memory successfully";
	
	dspace.close();
	memspace.close();
	dset.close();
	h5f.close();

        this->loaded = true;
        if (int_type) {
		if (ndims == 2) {
			return torch::from_blob(int_data, {(long int)dims[0], (long int)dims[1]}, torch::kInt8);
		} else if (ndims == 3) {
	        	return torch::from_blob(int_data, {(long int)dims[0], (long int)dims[1], (long int)dims[2]}, torch::kInt8);
		}
        }
	if (ndims == 2) {
		return torch::from_blob(float_data, {(long int)dims[0], (long int)dims[1]}, torch::kFloat32);
	} else if (ndims == 3) {
		return torch::from_blob(float_data, {(long int)dims[0], (long int)dims[1], (long int)dims[2]}, torch::kFloat32);
	}
}

int MLDSDataset::load(const torch::Device &dev)
{
        if (this->loaded) 
                return 0;

	X = load_hdf5_ds_into_tensor(xs_name);
	if (errflag) {
		return -ENOMEM;
	}
        this->_x_width = X.size(ndims-1);

	Y = load_hdf5_ds_into_tensor(ys_name);
	if (errflag) {
		return -ENOMEM;
	}
        this->_y_width = Y.size(ndims-1);

	X = X.to(dev);
	Y = Y.to(dev);

	INFO << "Successfully loaded dataset of " << X.size(0) << " examples into memory.";
	return 0;
}

