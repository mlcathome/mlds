/*
 * MLDS : Machine Learning Dataset Generator
 *
 * A program to train neural networks using pytorch for the MCL@Home
 * volunteer distributed computing project.
 *
 * Copyright 2020-2021 MLC@Home
 * Author(s): John Clemens <clemej1@umbc.edu>
 */

#include <iostream>
#include <algorithm>
#include <filesystem>
#include <errno.h>
#include <sstream>
#include <math.h>
#ifndef _WIN32
#include <getopt.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <unistd.h>
#endif

#include <torch/torch.h>
#if GPU
#if ROCM
#include <ATen/hip/HIPContext.h>
#else
#include <ATen/cuda/CUDAContext.h>
#endif
#endif

#include <dataset.h>
#include <trainer.h>
#include <llog.h>
#include <status.h>
#include <models.h>

#ifndef MLDS_VERSION
#define MLDS_VERSION "unknown"
#endif

#ifndef PYTORCH_BRANCH
#define PYTORCH_BRANCH "unknown"
#endif

#ifndef _WIN32
extern char **environ;
#endif

static bool check_file_exists(std::string fn)
{
	std::ifstream f(fn.c_str());
        return f.good();
}

static void write_pct_done(std::string fn, double pct)
{
	std::ofstream f(fn.c_str());
        f << pct << std::endl;
        return;
}

void print_usage(void)
{
	std::cout << "  Trains a simple neural network model on the given dataset" << std::endl << std::endl;
	std::cout << "  Usage: mlds [<options>]" << std::endl << std::endl;
	std::cout << "  Options:" << std::endl;
	std::cout << "    -h/--help             : This help message" << std::endl;
    	std::cout << "    -c/--continue         : Continue a computation" << std::endl;
	std::cout << "    -V/--version          : Print version and exit" << std::endl;
	std::cout << "    -v/--verbose <int>    : Set verbosity level (0=all msgs, 3=only errors, default=1)" << std::endl;
	std::cout << "    -t/--threshold <float>: Validation loss threshold for early stopping (default 0.0001)" << std::endl;
	std::cout << "    -l/--lr <float>       : Learning rate (default 0.01)" << std::endl;
	std::cout << "    -m/--maxepoch <int>   : Maximum number of epochs (default=2048)" << std::endl;
	std::cout << "    -w/--hwidth <int>     : Width of the hidden layers in cells (default=12)" << std::endl;
	std::cout << "    -r/--rhidden <int>    : Number of stacked recursive layers (default 4)" << std::endl;
	std::cout << "    -b/--bhidden <int>    : Number of stacked backend linear layers (default 4)" << std::endl;
	std::cout << "    -a/--arch             : Netowrk architecture: \"GRU\", \"LSTM\", \"LENET5\", \"DENSE\" (default: \"GRU\")" << std::endl;
	std::cout << "    -p/--patience <int>   : Number of epochs val_loss must be below threshold before exiting (default = 1)" << std::endl;
	std::cout << "    -s/--batchsize <int>  : Size of batch for training and eval (default = 128)" << std::endl;
        std::cout << "    -d/--dropout <float>  : Dropout percentage for models that support dropout (default = 0.0)" << std::endl;
	return;
}

int main(int argc, char *argv[])
{
	unsigned int batch_size = 128;
	unsigned int verbose_level = LLOG_LEVEL_INFO;
	double val_loss_threshold = 0.0001;
	//unsigned int max_epochs = 2048;
  unsigned int max_epochs = 100;
	unsigned int max_train_retries = 10;
	unsigned int hidden_width = 12;
	unsigned int rhidden = 4;
	unsigned int bhidden = 4;
	unsigned int patience = 10;
	double learning_rate = 0.01;
	std::string model_type = "GRU";
	bool arg_err_flag = false;
	int ret;
	unsigned int start_epoch = 1;
	//unsigned int cur_patience = 0;
    	bool continue_computation = false;
	unsigned int nthreads = 1;
	unsigned int gpuid = 0;
        float dropout_pct = 0.0;

    	std::vector<std::string> raw_filenames = {
                        "dataset.hdf5",
                        "model.cfg",
                        "model-final.pt",
                        "model-input.pt",
                        "snapshot.pt",
			"model-best.pt",
                        //"stdout.txt", // handled in llog.cpp
                        };
    	std::vector<std::string> real_filenames;
	std::stringstream full_version;

#ifndef _WIN32
#if 0
	/*
	 * On some system/pytorch/compiler/arch combinations, pytorch
	 * doesn't respect the omp_set_num_threads() setting. However
	 * it does respect the OMP_NUM_THREADS environment variable.
	 * However, BOINC doesn't allow us to specify environment
	 * variables. So we need to test and set that environment
	 * variable and then re-exec ourselves.
	 *
	 * For future multi-threaded support, BOINC passes a "-nthreads X"
	 * argument on the command line. We need to read that here early
	 * before we re-exec and handle all the other arguments.
         *
         * XXXjc 7/2021: This can probabaly be removed as we now use
         * the wrapper which can set env vars.
	 */
	if (getenv("MLDS_IS_REEXECED") == NULL) {
		std::cerr << "DEBUG: Args: ";
		setenv("MLDS_IS_REEXECED", "true", 1);
        	for (int i = 0; i < argc; i++) {
			std::string arg = argv[i];
			std::cerr << arg << " ";
           		if (arg == "--nthreads") {
				try {
					nthreads = std::stoul(argv[i+1]);
				} catch (...) {
					std::cout << "Error setting nthreads, falling back to 1" << std::endl;
				}
			}
			if (arg == "--device") {
	                        try {
                                        gpuid = std::stoul(argv[i+1]);
                                } catch (...) {
                                        std::cout << "Error setting GPU device, using 0" << std::endl;
                                }
			}
        	}
		std::cerr << std::endl << "nthreads: " << nthreads << " gpudev: " << gpuid << std::endl;
		setenv("OMP_NUM_THREADS", std::to_string(nthreads).c_str(), 1);
#ifdef ROCM
		setenv("ROCBLAS_TENSILE_LIBPATH", ".", 1);
#endif
		//setenv("CUDA_VISIBLE_DEVICES", std::to_string(gpuid).c_str(), 1);
		std::cerr << "Re-exec()-ing to set environment correctly" << std::endl;
		ret = execve(argv[0], argv, environ);
		std::cerr << "Re-exec()-ing failed, but continuing anyway. Number of threads may be wrong" << std::endl;
	}
#endif
#endif

#ifndef _WIN32
    omp_set_num_threads(nthreads);
#endif
    at::set_num_threads(nthreads);
    torch::set_num_threads(nthreads);

#ifndef _WIN32
	struct utsname uts;
	std::string arch;
	std::string system;
	ret = uname(&uts);
	if (ret == 0) {
		system = uts.sysname;
		arch = uts.machine;
	} else {
		system = "non-posix";
		arch = "unknown";
	}
	full_version << MLDS_VERSION << " (" << system << "/" << arch << ")";
#else
	full_version << MLDS_VERSION << " (Windows/x64)";
#endif
#if GPU
	auto *devprops = at::cuda::getCurrentDeviceProperties();
        full_version << " (libTorch: " << PYTORCH_BRANCH << " GPU: " << devprops->name << ")";
#else
	full_version << " (libTorch: " << PYTORCH_BRANCH << ")";
#endif
	std::cerr << "Machine Learning Dataset Generator v" << full_version.str() << std::endl;
	std::cout << "Machine Learning Dataset Generator v" << full_version.str() << std::endl;

#ifndef _WIN32
	while (!arg_err_flag) {
		int c;
		int option_index = 0;
      		static struct option long_options[] = {
          				{"version",  no_argument, 	NULL, 'V'},
					{"help",     no_argument, 	NULL, 'h'},
                    			{"continue", no_argument,       NULL, 'c'},
          				{"verbose",  required_argument, NULL, 'v'},
					{"lr",       required_argument, NULL, 'l'},
					{"threshold",required_argument, NULL, 't'},
          				{"maxepoch", required_argument, NULL, 'm'},
          				{"hwidth",   required_argument, NULL, 'w'},
          				{"rhidden",  required_argument, NULL, 'r'},
					{"bhidden",  required_argument, NULL, 'b'},
					{"patience", required_argument, NULL, 'p'},
					{"batchsize",required_argument, NULL, 's'},
					{"arch",     required_argument, NULL, 'a'},
					{"nthreads", required_argument, NULL, 'z'},
					{"device",   required_argument, NULL, 'x'},
                                        {"dropout",  required_argument, NULL, 'd'},
          				{0, 0, 0, 0}
    		};

      		c = getopt_long (argc, argv, "hcVl:v:t:m:w:r:b:p:s:a:z:x:d:",long_options, &option_index);
		if (c == -1)
        		break;

      		switch (c) {
        		case 'h':
				print_usage();
				return 0;
				break;
			case 'V':
				return 0;
				break;
            		case 'c':
                		continue_computation = true;
                		break;
			case 'z':
				/* already handled before reexec, set and ignore for info purposes */
				try {
					nthreads = std::stoul(optarg);
				} catch (...) {https://www.cbssports.com/mlb/gametracker/live/MLB_20210510_BOS@BAL/
					std::cerr << "Error reading # threads from cmdline. Defaulting to 1" << std::endl;
				}
				break;
			case 'v':
  				try {
    				verbose_level = std::stoul(optarg);
					verbose_level = std::min(LLOG_LEVEL_ERROR, verbose_level);
    				} catch (...) {
					arg_err_flag = true;
	   				std::cerr << "Error parsing argument to verbose" << std::endl;
  				}
				break;
			case 't':
  				try {
    				val_loss_threshold = std::stod(optarg);
    				} catch (...) {
					arg_err_flag = true;
	   				std::cerr << "Error parsing argument to threshold" << std::endl;
  				}
				break;
			case 'l':
  				try {
    				learning_rate = std::stod(optarg);
    				} catch (...) {
					arg_err_flag = true;
	   				std::cerr << "Error parsing argument to learning rate" << std::endl;
  				}
				break;
			case 'm':
  				try {
   					max_epochs = std::stoul(optarg);
   				} catch (...) {
					arg_err_flag = true;
	   				std::cerr << "Error parsing argument to max_epochs" << std::endl;
  				}
				break;
			case 'w':
  				try {
   					hidden_width = std::stoul(optarg);
					hidden_width = std::max(1u, hidden_width);
   				} catch (...) {
					arg_err_flag = true;
    				std::cerr << "Error parsing argument to hidden_width" << std::endl;
  				}
				break;
			case 'r':
  				try {
    				rhidden = std::stoul(optarg);
					rhidden = std::max(1u, rhidden);
    			} catch (...) {
					arg_err_flag = true;
	    			std::cerr << "Error parsing argument to rhidden" << std::endl;
  				}
				break;
			case 'b':
  				try {
					bhidden = std::stoul(optarg);
					bhidden = std::max(0u, bhidden);
    			} catch (...) {
					arg_err_flag = true;
	    				std::cerr << "Error parsing argument to bhidden" << std::endl;
  				}
				break;
			case 'p':
  				try {
    				patience = std::stoul(optarg);
					patience = std::max(1u, patience);
    			} catch (...) {
					arg_err_flag = true;
	    			std::cerr << "Error parsing argument to patience" << std::endl;
  				}
				break;
			case 's':
  				try {
    				batch_size = std::stoul(optarg);
					batch_size = std::max(1u, batch_size);
    			} catch (...) {
					arg_err_flag = true;
					std::cerr << "Error parsing argument to batch_size" << std::endl;
  				}
				break;
			case 'a':
				model_type = optarg;
				break;
			case 'x':
				try {
					std::cerr << "device already set" << std::endl;
	                                //gpuid = std::stoul(optarg);
                        	} catch (...) {
                                        std::cerr << "Error parsing argument to gpuindex, defaulting to 0" << std::endl;
                                }
				break;

                        case 'd':
                                try {
                                        dropout_pct = std::stod(optarg);
                                } catch (...) {
                                        std::cerr << "Error parsing argument to dropout, defaulting to 0.0" << std::endl;
                                }
                                break;
    			default:
     				return -EINVAL;
				break;
    		}
    	}

	if (arg_err_flag) {
		print_usage();
		return -EINVAL;
	}
#else
        /* Stupid windows non-posix compliant crap */
        std::vector<std::string> args;
        for (int i = 0; i < argc; i++) {
                args.push_back(argv[i]);
        }
        if (std::find(args.begin(), args.end(), "-h") != args.end() ||
                std::find(args.begin(), args.end(), "--help") != args.end()) {
                print_usage();
                return 0;
        }
        if (std::find(args.begin(), args.end(), "-V") != args.end() ||
                std::find(args.begin(), args.end(), "--version") != args.end()) {
                return 0;
        }
        if (std::find(args.begin(), args.end(), "-c") != args.end() ||
                std::find(args.begin(), args.end(), "--continue") != args.end()) {
                continue_computation = true;
        }
        for (int i = 1; i < args.size(); i++) {
                if (args[i] == "-v" || args[i] == "--verbose")
                        verbose_level = std::stoul(args[i+1]);
		if (args[i] == "-a" || args[i] == "--arch")
			model_type = std::string(args[i+1]);
		if (args[i] == "-z" || args[i] == "--nthreads")
			nthreads = std::stoul(argv[i+1]);
		if (args[i] == "-x" || args[i] == "--device")
			gpuid = std::stoul(argv[i+1]);
                if (args[i] == "-t" || args[i] == "--threshold")
                        val_loss_threshold = std::stod(args[i+1]);
                if (args[i] == "-m" || args[i] == "--maxepoch")
                        max_epochs = std::stoul(args[i+1]);
                if (args[i] == "-w" || args[i] == "--hwidth")
                        hidden_width = std::stoul(args[i+1]);
                if (args[i] == "-r" || args[i] == "--rhidden")
                        rhidden = std::stoul(args[i+1]);
                if (args[i] == "-b" || args[i] == "--bhidden")
                        bhidden = std::stoul(args[i+1]);
                if (args[i] == "-p" || args[i] == "--patience")
                        patience = std::stoul(args[i+1]);
                if (args[i] == "-s" || args[i] == "--batchsize")
                        batch_size = std::stoul(args[i+1]);
		if (args[i] == "-l" || args[i] == "--lr")
			learning_rate = std::stod(args[i+1]);
 		if (args[i] == "-d" || args[i] == "--dropout")
			dropout_pct = std::stod(args[i+1]);
               }
#endif

	at::set_num_threads(nthreads);
    	torch::set_num_threads(nthreads);

	llog::log_level = verbose_level;
	INFO << "Set logging level to " << llog::log_level;

    for (auto &fn : raw_filenames) {
        real_filenames.push_back(fn);
    }

#ifdef GPU
	torch::Device dev(torch::kCUDA, gpuid);
#else
	torch::Device dev(torch::kCPU);
#endif

	INFO << "Dataset filename: " << real_filenames[0];

	INFO << "Configuration: ";
	INFO << "    Model type: " << model_type;
	INFO << "    Validation Loss Threshold: " << val_loss_threshold;
	INFO << "    Max Epochs: " << max_epochs;
	INFO << "    Batch Size: " << batch_size;
	INFO << "    Learning Rate: " << learning_rate;
	INFO << "    Patience: " << patience;
	INFO << "    Hidden Width: " << hidden_width;
	INFO << "    # Recurrent Layers: " << rhidden;
	INFO << "    # Backend Layers: " << bhidden;
	INFO << "    # Threads: " << nthreads;
        INFO << "    Dropout: " << dropout_pct;

	INFO << "Preparing Dataset";
	int ds_ndims = 3;

        /* Assume for the moment that LENET and DENSE are 2d datasets.. nsamples x 1d array */
	if (model_type == "LENET5" || model_type == "DENSE") {
		ds_ndims = 2;
        }

	MLDSDataset train_ds(real_filenames[0], "/Xt", "/Yt", ds_ndims);
	MLDSDataset val_ds(real_filenames[0], "/Xv", "/Yv", ds_ndims);
    	train_ds.load(dev);
    	val_ds.load(dev);

	std::shared_ptr<MLDSModelImpl> model;
        std::shared_ptr<MLDSModelImpl> best = nullptr;

    	INFO << "Creating Model";
	if (model_type == "LSTM") {
    	        model = std::make_shared<BitMachineModelLSTMImpl>(
                        	train_ds.x_width(),train_ds.y_width(), hidden_width, rhidden, bhidden);
	} else if (model_type == "GRU") {
		model = std::make_shared<BitMachineModelGRUImpl>(
                    		train_ds.x_width(),train_ds.y_width(), hidden_width, rhidden, bhidden);
	} else if (model_type == "LENET5") {
		model = std::make_shared<ModdedLeNet5Impl>(1, dropout_pct);
        } else if (model_type == "DENSE") {
                model = std::make_shared<DenseImpl>(train_ds.x_width(), train_ds.y_width(), hidden_width, bhidden, dropout_pct);
	} else {
		LERROR << "Unknown model type " << model_type << std::endl;
		return -EINVAL;
	}
	best = model;


    INFO << "Preparing config file";
    MetaStatus status(real_filenames[1]);

    if (check_file_exists(real_filenames[4]) && status.exists()) {
            INFO << "Found checkpoint, attempting to load... ";
            INFO << "Loading config";
            status.load();
            INFO << "Loading state";
            torch::load(model, real_filenames[4]);
            DEBUG << "checkpoint loaded from disk";
            start_epoch = status.completed_epochs()+1;
    } else {
            INFO << "Creating new config file";
            status.version = full_version.str();
            status.dataset_filename = real_filenames[0].c_str();
			status.model_type = model_type;
            status.batch_size = batch_size;
            status.val_loss_threshold = val_loss_threshold;
            status.max_epochs = max_epochs;
            status.hidden_width = hidden_width;
            status.rhidden = rhidden;
            status.bhidden = bhidden;
            status.input_width = train_ds.x_width();
            status.output_width = train_ds.y_width();
			status.nthreads = nthreads;
            status.dropout_pct = dropout_pct;
#ifdef GPU
			status.gpu = true;
#endif
            status.save();

            if (continue_computation) {
                INFO << "This is a continuation WU, loading previous network";
                if (!check_file_exists(real_filenames[3].c_str())) {
                    LERROR << "Continuation requested, but model not found!";
                    return -EINVAL;
                }
                torch::load(model, real_filenames[3]);
            DEBUG << "model loaded from disk";
        }
    }

#ifdef GPU
    	model->to(dev);
#endif

	torch::optim::Adam *optimizer = new torch::optim::Adam(model->parameters(), torch::optim::AdamOptions(learning_rate));
	//torch::optim::SGD optimizer(model->parameters(), torch::optim::SGDOptions(0.01).momentum(0.9).nesterov(true));
	//torch::optim::LBFGS optimizer(model->parameters());
	torch::nn::MSELoss mse;
	torch::nn::CrossEntropyLoss cross;

	TrainerBase *trainer = NULL;
	trainer = new Trainer<MLDSModelImpl, torch::optim::Optimizer, torch::nn::MSELoss, MLDSDataset>
				(model, optimizer, mse, train_ds, val_ds, start_epoch);
	if (model_type == "LENET5" || model_type == "DENSE") {
		delete trainer;
		trainer = new Trainer<MLDSModelImpl, torch::optim::Optimizer, torch::nn::CrossEntropyLoss, MLDSDataset>
				(model, optimizer, cross, train_ds, val_ds, start_epoch);
	}

	INFO << "Loading DataLoader into Memory";
	trainer->load(batch_size, 0, dev);

	INFO << "Starting Training"; //on model with " << "(x)" << " parameters... ";

	double train_loss = 9999.0;
	double val_loss = 9999.0;
  double best_val_loss = 9999.0;
	int retries = 0;
	std::string prev_model = model->serialize_str();
	std::chrono::high_resolution_clock::time_point e_start, e_end;
	for(unsigned int epoch=start_epoch; epoch <= max_epochs; epoch++) {
		write_pct_done("pctdone.txt", (double)((double)(epoch-1)/(double)(max_epochs)));

		do {
			e_start = std::chrono::high_resolution_clock::now();
			train_loss = trainer->train(dev);
			val_loss = trainer->validate(dev);
			e_end = std::chrono::high_resolution_clock::now();

			if (isnan(train_loss) || isnan(val_loss)) {
				INFO << "Detected a NaN during training, reverting to previous epoch model";
				// Move the model to the CPU so we can modify it
				model->to(torch::kCPU);
				// De-serialise the model from the previous epoch into the new one
				model->load_str(prev_model);
				// Because the parameters have changed, the optimizer needs to be reset
				torch::optim::Adam *new_opt = new torch::optim::Adam(model->parameters(), torch::optim::AdamOptions(learning_rate));
				trainer->set_opt(new_opt);
#ifdef GPU
				// Move the model back to the device for processing
				model->to(dev);
#endif
				retries++;
			}
		} while ((isnan(train_loss) || isnan(val_loss)) && retries < max_train_retries);

		if (retries == max_train_retries) {
			LERROR << "Too many retries on erroneous model, exiting...";

			return 100; // Return a non-zero error code
		}

		retries = 0;

		if (model->name() == "ModdedLeNet5Impl" || model->name() == "DenseImpl") {
			INFO << "Epoch " << epoch << " | loss: " << train_loss << " | val_loss: " << val_loss << " | val acc: " << 100.0*trainer->get_accuracy() << " | Time: " <<
							std::chrono::duration<double, std::milli>(e_end-e_start).count()  << " ms";
		} else {
			INFO << "Epoch " << epoch << " | loss: " << train_loss << " | val_loss: " << val_loss << " | Time: " <<
				                         std::chrono::duration<double, std::milli>(e_end-e_start).count()  << " ms";
		}

		status.append_train_hist(train_loss);
		status.append_val_hist(val_loss);

    if (val_loss < best_val_loss) {
			best_val_loss = val_loss;
			DEBUG << "Saving new best model: loss == " << best_val_loss;
			model->to(torch::kCPU);
			torch::save(model, real_filenames[5]);
			//best.reset();
			//best = model->local_copy();
    }


    DEBUG << "Saving Snapshot";
		model->to(torch::kCPU);
    torch::save(model, real_filenames[4]);
		DEBUG << "Serializing new model to memory";
		prev_model = model->serialize_str();
		#ifdef GPU
			model->to(dev);
		#endif
    status.save();
	}

        INFO << "Last val_loss: " << val_loss;
	INFO << "Saving trained model to model-final.pt, best_val_loss " << best_val_loss ;
	//best->to(torch::kCPU);
	//torch::save(best, real_filenames[2]);

	/*
		If an incorrect template was used and on some rare occasion, the final model
		file will (for some reason) exist. Hence a warning should be printed but the
		results should still be written to the file no matter what.
	*/
	std::filesystem::path final_model_path{real_filenames[2]};
	if (std::filesystem::exists(final_model_path)) {
		WARN << "The final model file already exists. Has the right template been provided for this work unit?";
		INFO << "Overwriting the final model file...";
	}

	// Copy the best model to the final model in preparation for completion
	std::filesystem::copy(real_filenames[5], real_filenames[2], std::filesystem::copy_options::overwrite_existing);


        INFO << "Saving end state to config to file";
        status.save();

	write_pct_done("pctdone.txt", 100.0);
	INFO << "Success, exiting..";

	return 0;
}
