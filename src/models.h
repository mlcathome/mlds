#include <torch/torch.h>

#ifndef __MLDS_MODELS_H__
#define __MLDS_MODELS_H__

struct MLDSModelImpl : public torch::nn::Module {
	virtual torch::Tensor forward(torch::Tensor x) { torch::Tensor t; return t; };
  virtual std::shared_ptr<MLDSModelImpl> local_copy(void) { return nullptr; };
	virtual std::string serialize_str(void) { return ""; };
	virtual void load_str(std::string s) { return; };
};

/*
 * Create a model that can be trained on each of the
 * bitmachine models. 9 input pins, with memory, and
 * eight outputs. Note for some machines, only
 * a single output is active.  Use GRU and no
 * activations (linear).
 */
struct BitMachineModelGRUImpl : MLDSModelImpl {
	BitMachineModelGRUImpl(int input_size, int output_size,
			int hidden_width,
			int recursive_layers,
			int backend_layers) {

                blayers = std::min(backend_layers, 4);
		recurrent = register_module("recurrent",
				torch::nn::GRU(
					torch::nn::GRUOptions(input_size,hidden_width)
						.num_layers(recursive_layers)
						.batch_first(true)));
		l1 = register_module("l1", torch::nn::Linear(hidden_width, hidden_width));
		l2 = register_module("l2", torch::nn::Linear(hidden_width, hidden_width));
		l3 = register_module("l3", torch::nn::Linear(hidden_width, hidden_width));
		l4 = register_module("l4", torch::nn::Linear(hidden_width, hidden_width));
		output = register_module("output",
					torch::nn::Linear(hidden_width, output_size));
	}

	torch::Tensor forward(torch::Tensor x) {
		auto out = std::get<0>(recurrent->forward(x));
                if (blayers > 0)
		        out = l1->forward(out);
                if (blayers > 1)
		        out = l2->forward(out);
		if (blayers > 2)
                        out = l3->forward(out);
		if (blayers > 3)
                        out = l4->forward(out);

		out = output->forward(out);
		return out;
	}

        std::shared_ptr<MLDSModelImpl> local_copy(void) {
                auto retval = std::make_shared<MLDSModelImpl>();
                std::string data;

	        {
		        std::ostringstream oss;
		        torch::serialize::OutputArchive archive;

		        this->save(archive);
		        archive.save_to(oss);
		        data = oss.str();
	        }

                {
		        std::istringstream iss(data);
		        torch::serialize::InputArchive archive;
		        archive.load_from(iss);
		        retval->load(archive);
	        }

	        return retval;
        }

	std::string serialize_str(void) {
		std::string data;

		std::ostringstream oss;
		torch::serialize::OutputArchive archive;

		this->save(archive);
		archive.save_to(oss);
		data = oss.str();

		return data;
	}

	void load_str(std::string s) {
		std::istringstream iss(s);
		torch::serialize::InputArchive archive;
		archive.load_from(iss);

		this->load(archive);
	}


	/*
	 * See https://pytorch.org/tutorials/advanced/cpp_frontend.html
	 * for why nullptr.
	 */
        int                     blayers = 4;
	torch::Tensor		init_hidden;
	torch::nn::GRU		recurrent{nullptr};
	torch::nn::Linear 	l1{nullptr};
	torch::nn::Linear 	l2{nullptr};
	torch::nn::Linear 	l3{nullptr};
	torch::nn::Linear 	l4{nullptr};
	torch::nn::Linear 	output{nullptr};

	//std::string model_type(void) { return "GRU"; };
};
TORCH_MODULE(BitMachineModelGRU);

/*
 * Same as above, only use LSTM and tanh activations instead.
 */
struct BitMachineModelLSTMImpl : MLDSModelImpl {
	BitMachineModelLSTMImpl(int input_size, int output_size,
			int hidden_width,
			int recursive_layers,
			int backend_layers) {

                blayers = std::min(backend_layers, 4);
		recurrent = register_module("recurrent",
				torch::nn::LSTM(
					torch::nn::LSTMOptions(input_size,hidden_width)
						.num_layers(recursive_layers)
						.batch_first(true)));
		l1 = register_module("l1", torch::nn::Linear(hidden_width, hidden_width));
		l2 = register_module("l2", torch::nn::Linear(hidden_width, hidden_width));
		l3 = register_module("l3", torch::nn::Linear(hidden_width, hidden_width));
		l4 = register_module("l4", torch::nn::Linear(hidden_width, hidden_width));
		output = register_module("output",
					torch::nn::Linear(hidden_width, output_size));
	}

	torch::Tensor forward(torch::Tensor x) {
		auto out = torch::tanh(std::get<0>(recurrent->forward(x)));//.index({torch::indexing::Slice(), -1});

		if (blayers > 0)
		    out = l1->forward(out);
                if (blayers > 1)
		    out = l2->forward(out);
		if (blayers > 2)
                    out = l3->forward(out);
		if (blayers > 3)
                    out = l4->forward(out);

		out = output->forward(out);
		return out;
	}

        std::shared_ptr<MLDSModelImpl> local_copy(void) {
                auto retval = std::make_shared<MLDSModelImpl>();
                std::string data;

	        {
		        std::ostringstream oss;
		        torch::serialize::OutputArchive archive;

		        this->save(archive);
		        archive.save_to(oss);
		        data = oss.str();
	        }

                {
		        std::istringstream iss(data);
		        torch::serialize::InputArchive archive;
		        archive.load_from(iss);
		        retval->load(archive);
	        }

	        return retval;
        }

	std::string serialize_str(void) {
		std::string data;

		std::ostringstream oss;
		torch::serialize::OutputArchive archive;

		this->save(archive);
		archive.save_to(oss);
		data = oss.str();

		return data;
	}

	void load_str(std::string s) {
		std::istringstream iss(s);
		torch::serialize::InputArchive archive;
		archive.load_from(iss);

		this->load(archive);
	}


	/*
	 * See https://pytorch.org/tutorials/advanced/cpp_frontend.html
	 * for why nullptr.
	 */
        int                     blayers = 4;
	torch::Tensor		init_hidden;
	torch::nn::LSTM		recurrent{nullptr};
	torch::nn::Linear 	l1{nullptr};
	torch::nn::Linear 	l2{nullptr};
	torch::nn::Linear 	l3{nullptr};
	torch::nn::Linear 	l4{nullptr};
	torch::nn::Linear 	output{nullptr};

	//std::string model_type(void) { return "LSTM"; };
};
TORCH_MODULE(BitMachineModelLSTM);

/*
 * A Convolution network suited for MNIST, from TrojAI project's
 * "ModdedLeNet5Net".
 */
struct ModdedLeNet5Impl : MLDSModelImpl {
        ModdedLeNet5Impl() {
                channels = 1;
                dropout_pct = 0.0;
                reset();
        }
        ModdedLeNet5Impl(int channels, double dropout_pct) {
                this->channels = channels;
                this->dropout_pct = dropout_pct;
                reset();
        }
	void reset() {
        	c1 = register_module("conv1", torch::nn::Conv2d(channels,6,1));
	        m1 = register_module("mp1", torch::nn::MaxPool2d(
        	            torch::nn::MaxPool2dOptions({2, 2}).stride(2)));
		d1 = register_module("d1", torch::nn::Dropout2d(dropout_pct));
	        c2 = register_module("conv2", torch::nn::Conv2d(6,16,5));
	        m2 = register_module("mp2", torch::nn::MaxPool2d(
        	            torch::nn::MaxPool2dOptions({2, 2}).stride(2)));
		d2 = register_module("d2", torch::nn::Dropout2d(dropout_pct));
	        c3 = register_module("conv3", torch::nn::Conv2d(16,120,5));

		fc = register_module("fc", torch::nn::Linear(120, 84));
		output = register_module("output", torch::nn::Linear(84, 10));
	}

	torch::Tensor forward(torch::Tensor x) {
		auto xi = x.reshape({-1, 1, 28, 28});
		//INFO << xi.size(0) << " " << xi.size(1) << " " <<xi.size(2) ;
		auto out = torch::relu(c1->forward(xi));
		//INFO <<"here.";
        	out = m1->forward(out);
		out = d1->forward(out);
        	out = torch::leaky_relu(c2->forward(out));

        	out = m2->forward(out);
		out = d2->forward(out);
	        out = torch::leaky_relu(c3->forward(out));

	        out = out.view({-1, 120});
	        out = torch::leaky_relu(fc->forward(out));
	        out = output->forward(out);
    		//INFO << out.size(0) << " " << out.size(1) << "---";
		//return out;
		//out =  torch::log_softmax(out, 1);
		//INFO << out.size(0) << " " << out.size(1) ;

		return out;
	}

        std::shared_ptr<MLDSModelImpl> local_copy(void) {
                auto retval = std::make_shared<MLDSModelImpl>();
                std::string data;

	        {
		        std::ostringstream oss;
		        torch::serialize::OutputArchive archive;

		        this->save(archive);
		        archive.save_to(oss);
		        data = oss.str();
	        }

                {
		        std::istringstream iss(data);
		        torch::serialize::InputArchive archive;
		        archive.load_from(iss);
		        retval->load(archive);
	        }

	        return retval;
        }

	std::string serialize_str(void) {
		std::string data;

		std::ostringstream oss;
		torch::serialize::OutputArchive archive;

		this->save(archive);
		archive.save_to(oss);
		data = oss.str();

		return data;
	}

	void load_str(std::string s) {
		std::istringstream iss(s);
		torch::serialize::InputArchive archive;
		archive.load_from(iss);

		this->load(archive);
	}

	/*
	 * See https://pytorch.org/tutorials/advanced/cpp_frontend.html
	 * for why nullptr.
	 */
        int channels            = 1;
        double dropout_pct      = 0.0;
	torch::nn::Conv2d	c1{nullptr};
    	torch::nn::Conv2d	c2{nullptr};
    	torch::nn::Conv2d	c3{nullptr};
   	torch::nn::MaxPool2d	m1{nullptr};
    	torch::nn::MaxPool2d	m2{nullptr};
	torch::nn::Linear 	fc{nullptr};
	torch::nn::Linear 	output{nullptr};
	torch::nn::Dropout2d	d1{nullptr};
	torch::nn::Dropout2d	d2{nullptr};

	//std::string model_type(void) { return "LENET5"; };

};
TORCH_MODULE(ModdedLeNet5);

/*
 * Generic Dense Network
 *
 * This is an immensely silly way to do this, we should be able to use
 * ModuleList like in Python. But I don't see a way to both initialize
 * the layers with {nullptr} first.  This will work up to 20 hidden 
 * layers. 
 */
struct DenseImpl : MLDSModelImpl {
        DenseImpl(int input_width, int output_width, int hidden_width, int nhidden, float dropout_pct) :
                        input_width(input_width), output_width(output_width), hidden_width(hidden_width),
                        nhidden(nhidden), channels(1), dropout_pct(dropout_pct) {
                reset();
        }
        DenseImpl(int channels, int input_width, int output_width, int hidden_width, int nhidden, float dropout_pct) :
                        input_width(input_width), output_width(output_width), hidden_width(hidden_width),
                        nhidden(nhidden), channels(channels), dropout_pct(dropout_pct) {
                reset();
        }
	void reset() {
                input = register_module("input", torch::nn::Linear(input_width, hidden_width));
                output = register_module("output", torch::nn::Linear(hidden_width, output_width));
                h1 = (nhidden > 0)?register_module("h1", torch::nn::Linear(hidden_width, hidden_width)):h1;
                h2 = (nhidden > 1)?register_module("h2", torch::nn::Linear(hidden_width, hidden_width)):h2;
                h3 = (nhidden > 2)?register_module("h3", torch::nn::Linear(hidden_width, hidden_width)):h3;
                h4 = (nhidden > 3)?register_module("h4", torch::nn::Linear(hidden_width, hidden_width)):h4;
                h5 = (nhidden > 4)?register_module("h5", torch::nn::Linear(hidden_width, hidden_width)):h5;
                h6 = (nhidden > 5)?register_module("h6", torch::nn::Linear(hidden_width, hidden_width)):h6;
                h7 = (nhidden > 6)?register_module("h7", torch::nn::Linear(hidden_width, hidden_width)):h7;
                h8 = (nhidden > 7)?register_module("h8", torch::nn::Linear(hidden_width, hidden_width)):h8;
                h9 = (nhidden > 8)?register_module("h9", torch::nn::Linear(hidden_width, hidden_width)):h9;
                h10 = (nhidden > 9)?register_module("h10", torch::nn::Linear(hidden_width, hidden_width)):h10;
                h11 = (nhidden > 10)?register_module("h11", torch::nn::Linear(hidden_width, hidden_width)):h11;
                h12 = (nhidden > 11)?register_module("h12", torch::nn::Linear(hidden_width, hidden_width)):h12;
                h13 = (nhidden > 12)?register_module("h13", torch::nn::Linear(hidden_width, hidden_width)):h13;
                h14 = (nhidden > 13)?register_module("h14", torch::nn::Linear(hidden_width, hidden_width)):h14;
                h15 = (nhidden > 14)?register_module("h15", torch::nn::Linear(hidden_width, hidden_width)):h15;
                h16 = (nhidden > 15)?register_module("h16", torch::nn::Linear(hidden_width, hidden_width)):h16;
                h17 = (nhidden > 16)?register_module("h17", torch::nn::Linear(hidden_width, hidden_width)):h17;
                h18 = (nhidden > 17)?register_module("h18", torch::nn::Linear(hidden_width, hidden_width)):h18;
                h19 = (nhidden > 18)?register_module("h19", torch::nn::Linear(hidden_width, hidden_width)):h19;
                h20 = (nhidden > 19)?register_module("h20", torch::nn::Linear(hidden_width, hidden_width)):h20;
                d1 = (nhidden > 0)?register_module("d1", torch::nn::Dropout(dropout_pct)):d1;
                d2 = (nhidden > 1)?register_module("d2", torch::nn::Dropout(dropout_pct)):d2;
                d3 = (nhidden > 2)?register_module("d3", torch::nn::Dropout(dropout_pct)):d3;
                d4 = (nhidden > 3)?register_module("d4", torch::nn::Dropout(dropout_pct)):d4;
                d5 = (nhidden > 4)?register_module("d5", torch::nn::Dropout(dropout_pct)):d5;
                d6 = (nhidden > 5)?register_module("d6", torch::nn::Dropout(dropout_pct)):d6;
                d7 = (nhidden > 6)?register_module("d7", torch::nn::Dropout(dropout_pct)):d7;
                d8 = (nhidden > 7)?register_module("d8", torch::nn::Dropout(dropout_pct)):d8;
                d9 = (nhidden > 8)?register_module("d9", torch::nn::Dropout(dropout_pct)):d9;
                d10 = (nhidden > 9)?register_module("d10", torch::nn::Dropout(dropout_pct)):d10;
                d11 = (nhidden > 10)?register_module("d11", torch::nn::Dropout(dropout_pct)):d11;
                d12 = (nhidden > 11)?register_module("d12", torch::nn::Dropout(dropout_pct)):d12;
                d13 = (nhidden > 12)?register_module("d13", torch::nn::Dropout(dropout_pct)):d13;
                d14 = (nhidden > 13)?register_module("d14", torch::nn::Dropout(dropout_pct)):d14;
                d15 = (nhidden > 14)?register_module("d15", torch::nn::Dropout(dropout_pct)):d15;
                d16 = (nhidden > 15)?register_module("d16", torch::nn::Dropout(dropout_pct)):d16;
                d17 = (nhidden > 16)?register_module("d17", torch::nn::Dropout(dropout_pct)):d17;
                d18 = (nhidden > 17)?register_module("d18", torch::nn::Dropout(dropout_pct)):d18;
                d19 = (nhidden > 18)?register_module("d19", torch::nn::Dropout(dropout_pct)):d19;
                d20 = (nhidden > 19)?register_module("d20", torch::nn::Dropout(dropout_pct)):d20;
	
	}

	torch::Tensor forward(torch::Tensor x) {
		auto out = torch::leaky_relu(input->forward(x));
                out = (nhidden > 0)?torch::leaky_relu(h1->forward(d1->forward(out))):out;
                out = (nhidden > 1)?torch::leaky_relu(h2->forward(d2->forward(out))):out;
                out = (nhidden > 2)?torch::leaky_relu(h3->forward(d3->forward(out))):out;
                out = (nhidden > 3)?torch::leaky_relu(h4->forward(d4->forward(out))):out;
                out = (nhidden > 4)?torch::leaky_relu(h5->forward(d5->forward(out))):out;
                out = (nhidden > 5)?torch::leaky_relu(h6->forward(d6->forward(out))):out;
                out = (nhidden > 6)?torch::leaky_relu(h7->forward(d7->forward(out))):out;
                out = (nhidden > 7)?torch::leaky_relu(h8->forward(d8->forward(out))):out;
                out = (nhidden > 8)?torch::leaky_relu(h9->forward(d9->forward(out))):out;
                out = (nhidden > 9)?torch::leaky_relu(h10->forward(d10->forward(out))):out;
                out = (nhidden > 10)?torch::leaky_relu(h11->forward(d11->forward(out))):out;
                out = (nhidden > 11)?torch::leaky_relu(h12->forward(d12->forward(out))):out;
                out = (nhidden > 12)?torch::leaky_relu(h13->forward(d13->forward(out))):out;
                out = (nhidden > 13)?torch::leaky_relu(h14->forward(d14->forward(out))):out;
                out = (nhidden > 14)?torch::leaky_relu(h15->forward(d15->forward(out))):out;
                out = (nhidden > 15)?torch::leaky_relu(h16->forward(d16->forward(out))):out;
                out = (nhidden > 16)?torch::leaky_relu(h17->forward(d17->forward(out))):out;
                out = (nhidden > 17)?torch::leaky_relu(h18->forward(d18->forward(out))):out;
                out = (nhidden > 18)?torch::leaky_relu(h19->forward(d19->forward(out))):out;
                out = (nhidden > 19)?torch::leaky_relu(h20->forward(d20->forward(out))):out;
	        out = output->forward(out);

		return out;
	}

        std::shared_ptr<MLDSModelImpl> local_copy(void) {
                auto retval = std::make_shared<MLDSModelImpl>();
                std::string data;

	        {
		        std::ostringstream oss;
		        torch::serialize::OutputArchive archive;

		        this->save(archive);
		        archive.save_to(oss);
		        data = oss.str();
	        }

                {
		        std::istringstream iss(data);
		        torch::serialize::InputArchive archive;
		        archive.load_from(iss);
		        retval->load(archive);
	        }
		
	        return retval;
        }


	/* 
	 * See https://pytorch.org/tutorials/advanced/cpp_frontend.html
	 * for why nullptr.
	 */
        int channels            = 1;
        int input_width         = 1;
        int output_width        = 1;
        int hidden_width        = 1;
        int nhidden             = 1;
        float dropout_pct       = 0.0;
        torch::nn::Linear       input{nullptr};
        torch::nn::Linear 	h1{nullptr};
	torch::nn::Linear 	h2{nullptr};
	torch::nn::Linear 	h3{nullptr};
	torch::nn::Linear 	h4{nullptr};
	torch::nn::Linear 	h5{nullptr};
	torch::nn::Linear 	h6{nullptr};
	torch::nn::Linear 	h7{nullptr};
	torch::nn::Linear 	h8{nullptr};
	torch::nn::Linear 	h9{nullptr};
	torch::nn::Linear 	h10{nullptr};
	torch::nn::Linear 	h11{nullptr};
	torch::nn::Linear 	h12{nullptr};
	torch::nn::Linear 	h13{nullptr};
	torch::nn::Linear 	h14{nullptr};
	torch::nn::Linear 	h15{nullptr};
	torch::nn::Linear 	h16{nullptr};
	torch::nn::Linear 	h17{nullptr};
	torch::nn::Linear 	h18{nullptr};
	torch::nn::Linear 	h19{nullptr};
	torch::nn::Linear 	h20{nullptr};
        torch::nn::Dropout 	d1{nullptr};
	torch::nn::Dropout 	d2{nullptr};
	torch::nn::Dropout 	d3{nullptr};
	torch::nn::Dropout 	d4{nullptr};
	torch::nn::Dropout 	d5{nullptr};
	torch::nn::Dropout 	d6{nullptr};
	torch::nn::Dropout 	d7{nullptr};
	torch::nn::Dropout 	d8{nullptr};
	torch::nn::Dropout 	d9{nullptr};
	torch::nn::Dropout 	d10{nullptr};
	torch::nn::Dropout 	d11{nullptr};
	torch::nn::Dropout 	d12{nullptr};
	torch::nn::Dropout 	d13{nullptr};
	torch::nn::Dropout 	d14{nullptr};
	torch::nn::Dropout 	d15{nullptr};
	torch::nn::Dropout 	d16{nullptr};
	torch::nn::Dropout 	d17{nullptr};
	torch::nn::Dropout 	d18{nullptr};
	torch::nn::Dropout 	d19{nullptr};
	torch::nn::Dropout 	d20{nullptr};
	
	torch::nn::Linear 	output{nullptr};

};
TORCH_MODULE(Dense);



#endif
