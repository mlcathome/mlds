#include <vector>

#include <torch/torch.h>

#include <llog.h>

#ifndef __TRAINER_H__
#define __TRAINER_H__

class TrainerBase {
	public:
		virtual int load(int batch_size, int workers, const torch::Device &dev) { return 0; };
		virtual double train(const torch::Device &dev) { return 0.0; };
		virtual double validate(const torch::Device &dev) { return 0.0; };
		virtual int current_epoch(void) { return 0; }
		virtual std::vector<double> get_train_hist(void) { return std::vector<double>(); }
		virtual std::vector<double> get_val_hist(void) { return std::vector<double>(); }
		virtual double get_accuracy(void) { return 0.0; }
		virtual void set_opt(torch::optim::Optimizer *new_opt) { return; }
};

template<class M, class O, class C, class D>
class Trainer : public TrainerBase  {
	public:
		Trainer(std::shared_ptr<M> i_model, O *i_opt, C &i_crit, D &i_train, D &i_val, unsigned int start_epoch) :
					model(i_model), opt(i_opt), crit(i_crit),
					train_dataset(i_train), val_dataset(i_val),
       					train_loss(0.0), val_loss(0.0), epochs(start_epoch)
		{}

		int load(int batch_size, int workers, const torch::Device &dev)
		{
			int ret = -1;
			ret = train_dataset.load(dev);
			if (ret != 0) {
                		LERROR << "Failed to load dataset (ret = " << ret << ") exiting";
				return ret;
			}
			ret = val_dataset.load(dev);
			if (ret != 0) {
			    LERROR << "Failed to load dataset (ret = " << ret << ") exiting";
				return ret;
			}

			train_loader = torch::data::make_data_loader(
				train_dataset.map(torch::data::transforms::Stack<>()),
				torch::data::DataLoaderOptions().
					batch_size(batch_size).workers(workers));
			val_loader = torch::data::make_data_loader(
				val_dataset.map(torch::data::transforms::Stack<>()),
				torch::data::DataLoaderOptions().
					batch_size(batch_size).workers(workers));

			return 0;

		}

		// Set a new optimizer for the trainer
		void set_opt(O *new_opt)
		{
			delete opt; // Free the memory consumed by the previous optimizer
			opt = new_opt;
		}

		double train(const torch::Device &dev)
		{
			model->train();
			++epochs;

			DEBUG << "Epoch (Train) " << epochs;
			unsigned int batch_index = 0;
			double running_loss = 0.0;

			for(auto& batch : *train_loader) {
				auto prediction = model->forward(batch.data.toType(torch::kFloat).to(dev));
				opt->zero_grad();
				if (model->name() == "ModdedLeNet5Impl" || model->name() == "DenseImpl") {
					auto batch_loss = crit(prediction, std::get<1>(torch::max(batch.target.toType(torch::kFloat).to(dev), 1)));
					running_loss += batch_loss.template item<double>() * batch.data.size(0);
					batch_loss.backward();
				} else {
					auto batch_loss = crit(prediction, batch.target.toType(torch::kFloat).to(dev));
					running_loss += batch_loss.template item<double>() * batch.data.size(0);
					batch_loss.backward();
				}
				opt->step();

				if (++batch_index % 4 == 0) {
					DEBUG << " - Processed " << batch_index*batch.data.size(0) << " : loss " <<
						running_loss / (batch_index*batch.data.size(0));
				}
			}
			train_loss = running_loss / static_cast<double>(train_dataset.size().value());
			DEBUG << "- Final loss : " << train_loss;
                        hist_train.push_back(train_loss);
			return train_loss;
		}

		double validate(const torch::Device &dev)
		{
			model->eval();
			torch::NoGradGuard ngg;

			DEBUG << "Epoch (Validate) " << epochs;
			unsigned int batch_index = 0;
			double running_loss = 0.0;
			double total = 0.0;
			double correct = 0.0;
			this->accuracy = 0.0;

			for(auto& batch : *val_loader) {
				auto prediction = model->forward(batch.data.toType(torch::kFloat).to(dev));
				if (model->name() == "ModdedLeNet5Impl" || model->name() == "DenseImpl") {
					auto batch_loss = crit(prediction, std::get<1>(torch::max(batch.target.toType(torch::kFloat).to(dev), 1)));
					running_loss += batch_loss.template item<double>() * batch.data.size(0);
					total += batch.data.size(0);
					auto pred = prediction.argmax(1);
					//DEBUG << " -- " << pred << "\n" << "\n" << batch.target.argmax(1);
					correct += pred.eq(batch.target.argmax(1)).sum().template item<int64_t>();
					DEBUG << correct << " / " << total;
				} else {
					auto batch_loss = crit(prediction, batch.target.toType(torch::kFloat).to(dev));
					running_loss += batch_loss.template item<double>() * batch.data.size(0);
				}

				if (++batch_index % 4 == 0) {
					DEBUG << " - Processed " << batch_index*batch.data.size(0) << " : loss " <<
						running_loss / (batch_index*batch.data.size(0));
				}
			}
			val_loss = running_loss / static_cast<double>(val_dataset.size().value());
			this->accuracy = correct / total;
			if (model->name() == "ModdedLeNet5Impl" || model->name() == "DenseImpl") {
				DEBUG << " -- Accuracy: " << this->accuracy;
			}
			DEBUG << "- Final loss : " << val_loss;
			hist_val.push_back(val_loss);
			return val_loss;
		}

		int current_epoch(void) { return epochs; }

        std::vector<double> get_train_hist(void) { return hist_train; }
        std::vector<double> get_val_hist(void) { return hist_val; }
		double get_accuracy(void) { return accuracy; }


	private:
		std::shared_ptr<M> model;
		O *opt;
		C &crit;
		D &train_dataset;
		D &val_dataset;
		//torch::data::StatelessDataLoader<D, torch::data::samplers::RandomSampler> *train_loader = nullptr;
		//torch::data::StatelessDataLoader<D, torch::data::samplers::RandomSampler> *val_loader = nullptr;
std::unique_ptr<torch::data::StatelessDataLoader<torch::data::datasets::MapDataset<D, torch::data::transforms::Stack<torch::data::Example<> > >, torch::data::samplers::RandomSampler>, std::default_delete<torch::data::StatelessDataLoader<torch::data::datasets::MapDataset<D, torch::data::transforms::Stack<torch::data::Example<> > >, torch::data::samplers::RandomSampler> > > train_loader;
std::unique_ptr<torch::data::StatelessDataLoader<torch::data::datasets::MapDataset<D, torch::data::transforms::Stack<torch::data::Example<> > >, torch::data::samplers::RandomSampler>, std::default_delete<torch::data::StatelessDataLoader<torch::data::datasets::MapDataset<D, torch::data::transforms::Stack<torch::data::Example<> > >, torch::data::samplers::RandomSampler> > > val_loader;


		std::vector<double> hist_train;
		std::vector<double> hist_val;
		double train_loss;
		double val_loss;
		double accuracy;
		int epochs;
};

/*
 * I hate everything about C++ templates and the fact you *must* put the code in a header file.
 *
 * Its wrong and I hate it.
 */

#endif /* __TRAINER_H__ */
