#include <fstream>

#include "llog.h"

namespace llog {

unsigned int log_level = 2;

void log_to_stdout(const unsigned int level, const std::string &str) 
{
	if (level >= log_level) {
                std::cout << str << std::endl;
	}
	if (level >= LLOG_LEVEL_WARNING) {
		std::cerr << str << std::endl;
	}
	return;
}

InternalLog::InternalLog(unsigned int level, const std::string &func, const int line, 
			 LogFunctionType logFunction) : m_level(level), 
				m_logFunction(std::move(logFunction))
{
	if (level >= log_level) {
		auto log_time = std::chrono::system_clock::now();		
		std::time_t time = std::chrono::system_clock::to_time_t(log_time); 
		m_stringStream << "[" << std::put_time(std::localtime(&time), "%Y-%m-%d %X") 
			<< "\t" << std::setw(20) << func << ":" << line << "]" << "\t:\t";
		switch(level) {
			case LLOG_LEVEL_DEBUG:
				m_stringStream << "DEBUG";
				break;
			case LLOG_LEVEL_INFO:
				m_stringStream << "INFO";
				break;

			case LLOG_LEVEL_WARNING:
				m_stringStream << "WARN";
				break;

			case LLOG_LEVEL_ERROR:
				m_stringStream << "ERROR";
				break;
		}
		m_stringStream << "\t:\t";
	}
}


};
