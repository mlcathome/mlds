#include <iostream>
#include <fstream>
//#include <version>
#include <iomanip>

#if 0
#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    using fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

#if __GNUC__ &&__GNUC__ < 8
	#include <experimental/filesystem>
	using fs = std::experimental::filesystem;
#else
	#include <filesystem>
	using fs = std::filesystem;
#endif
#endif

#include <json.hpp>

#include "status.h"

using json = nlohmann::json;

MetaStatus::MetaStatus(std::string cfg_filename)
{
        config_filename = cfg_filename;
}

int MetaStatus::save(void)
{
        json j;
       
        j["version"] = version;
        j["dataset_filename"] = dataset_filename;
        j["model_type"] = model_type;
        j["batch_size"] = batch_size;
        j["val_loss_threshold"] = val_loss_threshold;
        j["max_epochs"] = max_epochs;
        j["hidden_width"] = hidden_width;
        j["rhidden"] = rhidden;
        j["bhidden"] = bhidden;
        j["patience"] = patience;
        j["input_width"] = input_width;
        j["output_width"] = output_width;
        j["train_loss_hist"] = train_hist;
        j["val_loss_hist"] = val_hist;
        j["nthreads"] = nthreads;
        j["learning_rate"] = learning_rate;
        j["gpu"] = gpu;
        j["dropout_pct"] = dropout_pct;

        std::ofstream o(config_filename);
        o << std::setprecision(10) << std::setw(4) << j;
        return 0;
}


int MetaStatus::load(void)
{
        json j;
        std::ifstream i(config_filename);
        i >> j;
       
        version = j["version"];
        dataset_filename = j["dataset_filename"];
        batch_size = j["batch_size"];
        val_loss_threshold = j["val_loss_threshold"];
        max_epochs = j["max_epochs"];
        hidden_width = j["hidden_width"];
        rhidden = j["rhidden"];
        bhidden = j["bhidden"];
        patience = j["patience"];
        input_width = j["input_width"];
        output_width = j["output_width"];
        train_hist = j["train_loss_hist"].get<std::vector<double>>();
        val_hist = j["val_loss_hist"].get<std::vector<double>>();
        if (j.contains("model_type")) {
                model_type = j["model_type"];
	}
	if (j.contains("nthreads")) {
                nthreads = j["nthreads"];
	}
	if (j.contains("learning_rate")) {
                learning_rate = j["learning_rate"];
        }
        if (j.contains("gpu")) {
                gpu = j["gpu"];
        }
        return 0;
}

int MetaStatus::exists(void)
{
//#if (__cplusplus >= 201703L) || (_MSVC_LANG >= 201703L)
//        return fs::exists(config_filename);
//#else
	std::ifstream f(config_filename.c_str());
        return f.good();
//#endif
}
