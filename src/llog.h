/*
 * llog.h : a lightweight logging library for C++ streams
 *
 * Adapted from basic concepts shown here: 
 *   https://stackoverflow.com/questions/511768/how-to-use-my-logging-class-like-a-std-c-stream
 *
 * Author: John Clemens <clemej1@umbc.edu> / <john@deater.net>
 *
 * Copyright (c) 2020: John Clemens
 */

#include <iostream>
#include <sstream>
#include <functional>
#include <iomanip>
#include <chrono>
#include <ctime>

#ifndef __LLOG_H__
#define __LLOG_H__

#define LLOG_LEVEL_DEBUG		0u
#define LLOG_LEVEL_INFO			1u
#define LLOG_LEVEL_WARNING		2u
#define LLOG_LEVEL_ERROR		3u

namespace llog {

/* 
 * Tail function that only logs the message. Called AFTER
 * other functions output the prefixes.
 */
void log_to_stdout(const unsigned int level, const std::string &str);

/*
 * Global log level. Set at init time;
 */
extern unsigned int log_level;

class InternalLog {
    using LogFunctionType = std::function<void(const unsigned int level, const std::string&)>;

public:
    InternalLog(unsigned int level, const std::string &func, const int line, LogFunctionType logFunction);
    std::ostringstream& stream() { return m_stringStream; }
    ~InternalLog() { m_logFunction(m_level, m_stringStream.str()); }

private:
    unsigned int m_level; 
    std::ostringstream m_stringStream;
    LogFunctionType m_logFunction;
};

};


#define DEBUG 	llog::InternalLog(LLOG_LEVEL_DEBUG,   __func__, __LINE__, llog::log_to_stdout).stream()
#define INFO	llog::InternalLog(LLOG_LEVEL_INFO,    __func__, __LINE__, llog::log_to_stdout).stream()
#define WARN	llog::InternalLog(LLOG_LEVEL_WARNING, __func__, __LINE__, llog::log_to_stdout).stream()
#define LERROR	llog::InternalLog(LLOG_LEVEL_ERROR,   __func__, __LINE__, llog::log_to_stdout).stream()

#endif /* __LLOG_H__ */
