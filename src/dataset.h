#include <torch/torch.h>

#ifndef __DATASET_H__
#define __DATASET_H__

class MLDSDataset : public torch::data::datasets::Dataset<MLDSDataset> {
	using Example = torch::data::Example<torch::Tensor, torch::Tensor>;


	public:
		MLDSDataset(const std::string &filename, const std::string &xs_str, const std::string &ys_str, int ndims);

		Example get(size_t index) {
			return {X[index], Y[index]};
		};
		torch::optional<size_t> size() const {
    			return X.size(0);
  		};
                
		int load(const torch::Device &dev);
	        unsigned int x_width(void) {return _x_width;}
	        unsigned int y_width(void) {return _y_width;}
	        bool int_type = false;

	private:
		bool errflag = false;
		torch::Tensor load_hdf5_ds_into_tensor(const std::string &dataset);
		std::string dataset_filename;

		std::string xs_name;
		std::string ys_name;
		torch::Tensor X, Y;
	        unsigned int _x_width;
        	unsigned int _y_width;
	        bool loaded = false;
		int ndims = 3;
};

#endif /* __DATASET_H__ */
