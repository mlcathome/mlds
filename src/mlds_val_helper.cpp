
#include <torch/torch.h>
#include <dataset.h>
#include <llog.h>
#include <status.h>
#include <models.h>

int main(int argc, char *argv[]) 
{
        double result;
	torch::Device dev(torch::kCPU);

        if (argc != 4 && argc != 3) {
                fprintf(stderr, "%s <model> <dataset> [<config>]\n", argv[0]);
                return -EINVAL;
        }
	std::string statusfile;

	statusfile = "none";
	if (argc == 4) {
		statusfile = argv[3];
	}
	MetaStatus status(statusfile);

	if (argc == 4) {
		status.load();
	}

        //mkl_set_num_threads(1);
        omp_set_num_threads(1);
        at::set_num_threads(1);
        torch::set_num_threads(1);

        //std::shared_ptr<BitMachineModel> ptr = std::make_shared<BitMachineModel>(
        //        input_size, output_size, hidden_width, nrecur, nbackend);
        llog::log_level = LLOG_LEVEL_ERROR;

	int ndims = 3;
	if (status.model_type == "LENET5" || status.model_type == "DENSE") {
		ndims=2;
	}

        MLDSDataset eval_ds(argv[2], "/Xs", "/Ys",ndims);
        eval_ds.load(dev);

		std::shared_ptr<MLDSModelImpl> ptr;
		if (status.model_type == "LSTM") {
    			ptr = std::make_shared<BitMachineModelLSTMImpl>(
                        	status.input_width,status.output_width, status.hidden_width, status.rhidden, status.bhidden);
		} else if (status.model_type == "GRU") {
			ptr = std::make_shared<BitMachineModelGRUImpl>(
                    		eval_ds.x_width(),eval_ds.y_width(), status.hidden_width, status.rhidden, status.bhidden);
		} else if (status.model_type == "LENET5") {
			ptr = std::make_shared<ModdedLeNet5Impl>(1, status.dropout_pct);
                } else if (status.model_type == "DENSE") {
                        ptr = std::make_shared<DenseImpl>(eval_ds.x_width(), eval_ds.y_width(), status.hidden_width, status.bhidden, status.dropout_pct);
		} else {
			LERROR << "Unknown model type " << status.model_type << std::endl;
			return -EINVAL;
		}

        torch::load(ptr, argv[1]);
        ptr->eval();

        auto eval_loader = torch::data::make_data_loader(
				eval_ds.map(torch::data::transforms::Stack<>()),
				torch::data::DataLoaderOptions().
					batch_size(1024).workers(0));
        
        torch::NoGradGuard ngg;
	double running_loss = 0.0;

	for(auto& batch : *eval_loader) {
		auto prediction = ptr->forward(batch.data.toType(torch::kFloat));
		if (status.model_type == "LENET5" || status.model_type == "DENSE") {
                        auto batch_loss = torch::nn::functional::cross_entropy(prediction, std::get<1>(torch::max(batch.target.toType(torch::kFloat).to(dev), 1)));
                        running_loss += batch_loss.item<double>() * batch.data.size(0);
		} else {
			auto batch_loss = torch::mse_loss(prediction, batch.target.toType(torch::kFloat));
			running_loss += batch_loss.item<double>() * batch.data.size(0);
		}
        }
	result = running_loss / static_cast<double>(eval_ds.size().value());
	DEBUG << "- Final loss : " << result;
#if 0
        if (result >= std::stod(argv[3])) {
                WARN << "Final loss " << result << " not below threshold " << std::stod(argv[3]);
                return -EINVAL;
        }
#endif
        std::cout << result << std::endl;
        return 0;
}

