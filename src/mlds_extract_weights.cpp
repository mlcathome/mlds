
#include <torch/torch.h>
#include <llog.h>
#include <models.h>
#include <status.h>

int main(int argc, char *argv[]) 
{
        if (argc != 2 && argc != 3) {
                fprintf(stderr, "%s <model> <config>\n", argv[0]);
                return -EINVAL;
        }
	std::string statusfile = "none";
	if (argc == 3) {
		statusfile = argv[2];
	}
	MetaStatus status(statusfile);
	if (argc == 3) {
		status.load();
	}
	//LERROR << status.version << " " << status.model_type << std::endl;

	std::shared_ptr<MLDSModelImpl> ptr;
	if (status.model_type == "LSTM") {
    		ptr = std::make_shared<BitMachineModelLSTMImpl>(
				status.input_width, status.output_width, status.hidden_width, status.rhidden, status.bhidden);

        } else if (status.model_type == "GRU") {
		ptr = std::make_shared<BitMachineModelGRUImpl>(
				    status.input_width, status.output_width, status.hidden_width, status.rhidden, status.bhidden);
	} else if (status.model_type == "LENET5" ) {
		ptr = std::make_shared<ModdedLeNet5Impl>(1, status.dropout_pct);
        } else if (status.model_type == "DENSE") {
                ptr = std::make_shared<DenseImpl>(status.input_width, status.output_width, status.hidden_width, status.bhidden, status.dropout_pct);
	} else {
		LERROR << "Unknown model type " << status.model_type << std::endl;
		return -EINVAL;
	}

        llog::log_level = LLOG_LEVEL_INFO;
    
        std::cout.precision(10);

        torch::load(ptr, argv[1]);
        int sum = 0;
        std::cout << "{" << std::endl;
        for (const auto& pair : ptr->named_parameters()) {
                std::cout << "\t\"" << pair.key() << "\": { " << std::endl;
                torch::Tensor p = torch::flatten(pair.value());
                auto pita = p.accessor<float,1>();
                std::cout << "\t\t\"size\": " << pita.size(0) << "," << std::endl;
                sum += pita.size(0);
                std::cout << "\t\t\"values\": [ ";
                for (int i=0; i<pita.size(0); i++) {
                        if (i != 0) 
                                std::cout << ", ";
                        std::cout << pita[i];
                }
                std::cout << " ]" << std::endl;
                std::cout << "\t}," << std::endl;
        }
        std::cout << "\t\"num_params\": " << sum << std::endl;
        std::cout << "}" << std::endl;
        return 0;
}


